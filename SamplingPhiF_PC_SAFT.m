function [zrho,f,phi]=SamplingPhiF_PC_SAFT(comps,Temp,x)
ADiMat_startup
% Konstanten:
load PC_koeff.mat
load PCP_koeff.mat

load np_sigma.mat
load np_epsilon.mat
load np_segmentZahl.mat
load np_miu.mat
load segs.mat

load np_kijA.mat
load np_kijB.mat

NC=length(comps) ;
sigma_i_alpha    = zeros(NC,2) ;
epsilon_i_alpha = zeros(NC,2) ;
m_i_alpha        = zeros(NC,2) ;
miu_i            = zeros(NC,2) ;
segvector        = zeros(NC,1) ;
for i=1:NC
   sigma_i_alpha(i,:)  = sigma(comps(i),:) ;
   epsilon_i_alpha(i,:)= epsilon(comps(i),:) ;
   m_i_alpha(i,:)      = m(comps(i),:) ;
   miu_i(i,:)          = miu(comps(i),:) ;
   segvector(i)        = segs(comps(i),:) ;
end

maxSegmentNumber = max(segvector) ;
k_ij_a = zeros(NC,maxSegmentNumber,NC,maxSegmentNumber) ;
k_ij_b = zeros(NC,maxSegmentNumber,NC,maxSegmentNumber) ;

for i = 1:NC
    for alpha=1:segvector(i)
        for j = 1:NC
            for beta = 1:segvector(j)
                k_ij_a(i,alpha,j,beta) =  kijA(2*(comps(i)-1)+alpha,2*(comps(j)-1)+beta) ;
                k_ij_b(i,alpha,j,beta) =  kijB(2*(comps(i)-1)+alpha,2*(comps(j)-1)+beta) ;
            end
        end
    end
end

k=1.38066e-23;
P = 1e5 ;

samplesize=length(Temp);
phi=zeros(samplesize,NC);
f=zeros(samplesize,NC);
zrho=zeros(samplesize,2);
for reihe=1:samplesize
    disp(reihe)
    T=Temp(reihe);
    
    % Temperatur-abh�ngige Konstante:
    k_ij = k_ij_a + T*k_ij_b ;
    
        options = optimset('Display','None','MaxFunEvals',1e6,...
                   'MaxIter',1e6,'Algorithm','trust-region-dogleg') ;
    options2 = optimset('Display','None','MaxFunEvals',1e8,...
                   'MaxIter',1e8,'Algorithm','trust-region-reflective');
    xiter1=x(reihe,:);
%Z_rho1 = fsolve(@(X) Z_rho(X,xiter1,m_i_alpha,T,sigma_i_alpha,...
%                               epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),...
%                               [0.005 0.005],options) ;
                           % Caution with initial points! Check during the
                           % sampling!!

%22.03.18: Add contstraints for avoiding outliers:
Z_rho1 = lsqnonlin(@(X) Z_rho(X,xiter1,m_i_alpha,T,sigma_i_alpha,...
                               epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),...
                               [0.002 0.008],[0.001,0.001],[0.01,0.01],options2) ; 	

    Z1  = Z_rho1(1) ;
    rho1= Z_rho1(2) ;

    phi1 = fuga_coeff(rho1,xiter1,m_i_alpha,NC,T,sigma_i_alpha,...
                epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z1) ;    
            
phi(reihe,:)=phi1;
f(reihe,:)=phi1.*xiter1;
zrho(reihe,:)=Z_rho1;
end
end