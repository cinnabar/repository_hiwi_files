function [x,fval,exitflag,info]=Decanter_optitool_PC_SAFT(xin,T,comps,x0)
%16.03.2018 Opti Tool Decanter using PC-SAFT-call

% adimat must be activated
% pc-saft files must be in path!

% xin must be a vector for ONE input composition xin(1:NC)
% T must be the scalar temperature in K

% IMPORTANT: Order of components in "comps"
% ternary:
% comps must be either [7,6,1] or [7,6,10] = decane(7),dmf(6),dodecene(1) or dodecanal(10)

% quaternary:
% comps must be [7,6,10,1] = decane(7),dmf(6),dodecanal(10),dodecene(1)

% x0 is the initial value. See in x0-section below for examples

%% Inputs for the function are: (x,xin,T,comps)

% Variable x:
% x(Phase1)= x(1:NC) 1-3
% x(Phase2)= x(NC+1:NC*2) 4-6
% Z(Phase1)=x(NC*2+1); 7
% rho(Phase1)=x(NC*2+2); 8
% Z(Phase2)=x(NC*2+3); 9
% rho(Phase2)=x(NC*2+4); 10
% K=x(NC*2+5)=x(end);

% Number of components
NC=length(comps);

%% Objective
fun = @(x) 1;

%% Bounds (lb <= x <= ub)
% Non-polar solvent decane (xin(1)) goes to non-polar phase: 
% bounds x(1): 0-xin(1), bounds x(5): xin(1)-1
% Polar solvent dmf (xin(2)) goes to polar phase: 
% bounds x(2): xin(2)-1, bounds x(6): 0-xin(2)
lb=[0;xin(2);zeros(NC-2,1); xin(1);0;zeros(NC-2,1); 0.001;0.001; 0.001;0.001; 0];
ub=[xin(1);1;ones(NC-2,1); 1;xin(2);ones(NC-2,1); 0.01;0.01; 0.01;0.01; 1];

%% PC-SAFT call
% Constants
load('PC_koeff.mat','A')
load('PCP_koeff.mat','B')
load('np_sigma.mat','sigma')
load('np_epsilon.mat','epsilon')
load('np_segmentZahl.mat','m')
load('np_miu.mat','miu')
load('segs.mat','segs')
load('np_kijA.mat','kijA')
load('np_kijB.mat','kijB')

sigma_i_alpha    = zeros(NC,2) ;
epsilon_i_alpha = zeros(NC,2) ;
m_i_alpha        = zeros(NC,2) ;
miu_i            = zeros(NC,2) ;
segvector        = zeros(NC,1) ;
for i=1:NC
   sigma_i_alpha(i,:)  = sigma(comps(i),:) ;
   epsilon_i_alpha(i,:)= epsilon(comps(i),:) ;
   m_i_alpha(i,:)      = m(comps(i),:) ;
   miu_i(i,:)          = miu(comps(i),:) ;
   segvector(i)        = segs(comps(i),:) ;
end

maxSegmentNumber = max(segvector) ;
k_ij_a = zeros(NC,maxSegmentNumber,NC,maxSegmentNumber) ;
k_ij_b = zeros(NC,maxSegmentNumber,NC,maxSegmentNumber) ;

for i = 1:NC
    for alpha=1:segvector(i)
        for j = 1:NC
            for beta = 1:segvector(j)
                k_ij_a(i,alpha,j,beta) =  kijA(2*(comps(i)-1)+alpha,2*(comps(j)-1)+beta) ;
                k_ij_b(i,alpha,j,beta) =  kijB(2*(comps(i)-1)+alpha,2*(comps(j)-1)+beta) ;
            end
        end
    end
end
k=1.38066e-23;
P = 1e5 ;

k_ij = k_ij_a + T*k_ij_b ;

%% Linear Equality Constraints
% Phase compositions must sum up to 1
Aeq = [ones(1,NC), -1*ones(1,NC), 0 0 0 0 0];
beq = [ 0 ];



%% Linear Inequality Constraints (Ax <= b)
% Minimum distance between x phase 1 and x phase 2
  Aieq = [1,zeros(1,NC-1), -1,zeros(1,NC-1),0 0 0 0 0];
  bieq = [ -0.05 ];
 
%% Nonlinear Equality Constraints
% Z and rho, Isofugacity, distribution coefficient K=x(end)

nlcon = @(x)[  ...
                [x(NC*2+1)-1-x(NC*2+2).*[a_res_diff(x(NC*2+2),x(1:NC),T,A,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,miu_i,B,k_ij,segvector)]'];... %Z-1-rho.*d_a_res_rho=0 for Phase 1
                [P/1e5-x(NC*2+1).*(k.*1e25)*T.*x(NC*2+2)];...    %P/1e5-Z.*(k.*1e25)*T.*rho=0 for Phase 1
                [x(NC*2+3)-1-x(NC*2+4).*[a_res_diff(x(NC*2+4),x(NC+1:NC*2),T,A,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,miu_i,B,k_ij,segvector)]'];...    %Z-1-rho.*d_a_res_rho=0 for Phase 2
                [P/1e5-x(NC*2+3).*(k.*1e25)*T.*x(NC*2+4)];...    %P/1e5-Z.*(k.*1e25)*T.*rho=0 for Phase 2
                [[fuga_coeff(x(NC*2+2),x(1:NC)',m_i_alpha,NC,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij,x(NC*2+1)).*x(1:NC)']-[fuga_coeff(x(NC*2+4),x(NC+1:NC*2)',m_i_alpha,NC,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij,x(NC*2+3)).*x(NC+1:NC*2)']]';... %f1-f2
                [x(end)*(x(NC+1:NC*2)-x(1:NC))-(x(NC+1:NC*2))+xin']];%K            

nlrhs = [   zeros(4,1);...
            zeros(NC,1);...
            zeros(NC,1)];
nle =   [   zeros(size(nlrhs))];        

%% Options
solvopts=ipoptset('constr_viol_tol',1e-8);
opts = optiset('solver','ipopt','display','iter','solverOpts',solvopts);

%% Set up optimization problem
Opt = opti('fun',fun,'bounds',lb,ub,'eq',Aeq,beq,'ineq',Aieq,bieq,'nlmix',nlcon,nlrhs,nle,'options',opts);      

%% Initial point
% Must be pre-defined!
% x0=[0.1;0.89;0.01;0.9;0.05;0.05;0.004;0.006;0.006;0.004;0.5];% ternary with Dodecene(1)
% x0=[0.1;0.89;0.01;0.9;0.05;0.05;0.004;0.006;0.006;0.004;0.5];% ternary with Dodecanal(10)

%% Solve
[x,fval,exitflag,info] = solve(Opt,x0);

%% PC-SAFT call
function d_a_res_rho=a_res_diff(rho1,xphase,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector)
opts = admOptions('i',[1]) ;
[d_a_res_rho,a_res]= admDiffComplex(@a_residual,1,rho1,xphase',T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector,opts) ;
end

end