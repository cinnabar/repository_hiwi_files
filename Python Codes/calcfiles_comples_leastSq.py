import numpy as np
import math
import cmath

def Z_rho(X,x,m_i_alfa,T,sigma_i_alfa,epsilon_i_alfa,segvector,A,k,P,B,miu_i,k_ij):
    Z=X[0]
    rho=X[1]
    n=x.shape[1] #No. of Components
    #print(Z,rho)
    d_a_res_rho=(complex_partial_derivative(a_residual,0,[rho,x,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector]))
    #print('\nd_a_res_rho:'+str(d_a_res_rho))
    F=np.zeros(2)
    F[0]=np.real(Z-1-rho*d_a_res_rho)
    F[1]=np.real(P/1e5-Z*(k*1e25)*T*rho)
    #print('\nF:'+str(F.shape))
    return F

def fuga_coeff(rho,x,m_i_alfa,n,T,sigma_i_alfa,epsilon_i_alfa,segvector,A,B,miu_i,k_ij,Z):
    d_a_res_x= complex_partial_derivative(a_residual,1,[rho,x,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector])
    a_res= a_residual([rho,x,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector])
    #print("\n\n d_a_res:"+str(d_a_res_x))
    #print("\n\n a_res:"+str(a_res))
    X = np.zeros((1,n),dtype=np.complex_)
    #X = np.zeros((1,n))
    for i in range(n):
        X[0,i]=x[0,i]*d_a_res_x[0,i]
    XX=np.sum(X)
    #print("\n\n XX:"+str(XX))
    phi=np.zeros((1,n),dtype=np.complex_)
    #phi=np.zeros((1,n))
    for i in range(n):
        fkoa=math.log(Z)
        #print("\n\n a_Res:"+str(a_res))
        #print("\n\n d_a_res_x:"+str(d_a_res_x[0,i]))
        phi[0,i]=cmath.exp(a_res+d_a_res_x[0,i]-XX+Z-1-fkoa)
    return phi

def a_residual(arguments):
    rho,x,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector=arguments
    n=x.shape[1]
    hetsegments = int(np.amax(segvector)) #Maximum number of segments used in the available components
    d_i_alfa=seg_diam(T,n,hetsegments,segvector,sigma_i_alfa,epsilon_i_alfa)
    m_i, m_i_mix=segmentCalcs(x,n,m_i_alfa)
    z_i_alfa=seg_frac(m_i,n,hetsegments,segvector,m_i_alfa)
    zeta_0,zeta_1,zeta_2,zeta_3 = pack_frac(rho,x,T,n,hetsegments, segvector,m_i,d_i_alfa,z_i_alfa)
    sigma_ij = np.zeros((n,hetsegments,n,hetsegments),dtype=np.complex_)
    epsilon_ij_dipol = np.zeros((n,hetsegments,n,hetsegments),dtype=np.complex_)
    epsilon_ij_disp  = np.zeros((n,hetsegments,n,hetsegments),dtype=np.complex_)
    #sigma_ij = np.zeros((n,hetsegments,n,hetsegments))
    #epsilon_ij_dipol = np.zeros((n,hetsegments,n,hetsegments))
    #epsilon_ij_disp  = np.zeros((n,hetsegments,n,hetsegments))
    for i in range(n):
        for p in range(int(segvector[0,i])):
            for j in range(n):
                for q in range(int(segvector[0,j])):
                    sigma_ij[i,p,j,q]=1/2*(sigma_i_alfa[i,p]+sigma_i_alfa[j,q])
                    epsilon_ij_dipol[i,p,j,q]=(epsilon_i_alfa[i,p]*epsilon_i_alfa[j,q])**0.5
                    epsilon_ij_disp[i,p,j,q]=epsilon_ij_dipol[i,p,j,q]*(1-k_ij[i,p,j,q])
    #print("\n\nd_i_alfa:"+str(d_i_alfa)+"\n\nm_i:"+str(m_i)+"\n\nm_i_mix:"+ str(m_i_mix)+"\n\nz_i_alfa:"+str(z_i_alfa)+"\n\nzeta_0:"+str(zeta_0)+"\n\nzeta_1:"+str(zeta_1)+"\n\nzeta_2:"+str(zeta_2)+"\n\nzeta_3:"+str(zeta_3))
    a_hc = a_hardchain(x,n,hetsegments,segvector,m_i,m_i_mix,m_i_alfa,d_i_alfa,zeta_0,zeta_1,zeta_2,zeta_3)#change to zeta_3
    a_disp = a_dispersion(rho,x,T,n,segvector,A,m_i,m_i_mix,sigma_ij,epsilon_ij_disp,z_i_alfa,zeta_3)
    a_dipol = a_dipole(rho,x,T,n,hetsegments,segvector, m_i, sigma_ij,epsilon_ij_dipol,z_i_alfa,miu_i,B,zeta_3)
    #print("\n a_hc:"+str(a_hc))
    #print("a_disp:"+str(a_disp))
    #print("a_dipol:"+str(a_dipol))
    a_res=a_hc + a_disp + a_dipol
    #print("a_res:"+str(a_res)+'\n')
    return a_res

def a_hardchain(x,n,hetsegments,segvector,m_i,m_i_mix,m_i_alfa,d_i_alfa,zeta_0,zeta_1,zeta_2,zeta_3):
    #print("\n\nx:"+str(x)+"\n\nn:"+str(n)+"\n\nhetsegments:"+str(hetsegments)+"\n\nsegvector:"+str(segvector)+"\n\nm_i:"+str(m_i)+"\n\nm_i_mix:"+str(m_i_mix)+"\n\nm_i_alfa:"+str(m_i_alfa)+"\n\nd_i_alfa:"+str(d_i_alfa)+"\n\nzeta_0:"+str(zeta_0))
    #print("\n\nzeta_1:"+str(zeta_1)+"\n\nzeta_2"+str(zeta_2)+"\n\nzeta_3"+str(zeta_3))
    a_hs=1/zeta_0*(3*zeta_1*zeta_2/(1-zeta_3)+zeta_2**3/(zeta_3*(1-zeta_3)**2)+(zeta_2**3/zeta_3**2-zeta_0)*np.emath.log(1-zeta_3))
    #print("\n\na_hs:"+str(a_hs))
    d_i  = np.zeros((n,hetsegments,hetsegments),dtype=np.complex_)
    g_hs = np.zeros((n,hetsegments,hetsegments),dtype=np.complex_)
    B_ii = np.zeros((n,hetsegments,hetsegments),dtype=np.complex_)
    X = np.zeros((n,hetsegments,hetsegments),dtype=np.complex_)
    #d_i  = np.zeros((n,hetsegments,hetsegments))
    #g_hs = np.zeros((n,hetsegments,hetsegments))
    #B_ii = np.zeros((n,hetsegments,hetsegments))
    #X = np.zeros((n,hetsegments,hetsegments))

    for i in range(n):
        for p in range(int(segvector[0,i])):
            for q in range(p,int(segvector[0,i])):
                d_i[i,p,q] = d_i_alfa[i,p]*d_i_alfa[i,q]/(d_i_alfa[i,p]+d_i_alfa[i,q])
                g_hs[i,p,q]=1/(1-zeta_3)+d_i[i,p,q]*3*zeta_2/(1-zeta_3)**2+d_i[i,p,q]**2*2*zeta_2**2/(1-zeta_3)**3
                B_ii[i,p,q]= 1/(np.sum(m_i_alfa[i,:])-1)
                B_ii[i,p,p]= (m_i_alfa[i,p]-1)/(np.sum(m_i_alfa[i,:])-1)
                X[i,p,q] = B_ii[i,p,q]*np.emath.log(g_hs[i,p,q])
    #print("\n\nX:"+str(X))
    #print("\n\nd_i:"+str(d_i))
    #print("\n\ng_hs:"+str(g_hs))
    #print("\n\nB_ii:"+str(B_ii))
    XX=np.sum(np.sum(X,2),1)
    Y = np.zeros((n,1),dtype=np.complex_)
    #Y = np.zeros((n,1))
    for i in range(n):
       Y[i] = x[0,i]*(m_i[i,0]-1)*XX[i]
    #print("\n\nY:"+str(Y))
    #print("\n\nXX:"+str(XX))

    a_hc = m_i_mix*a_hs-np.sum(Y)
    #print("a_hc:"+str(a_hc)+"\n")
    return a_hc

def seg_diam(T,n,hetsegments,segvector,sigma_i_alfa,epsilon_i_alfa):
    d_i_alpha=np.zeros((n,hetsegments),dtype=np.complex_)
    #d_i_alpha=np.zeros((n,hetsegments))
    #print("T:"+str(T)+"\n")
    #print("sigma_i_alfa:"+str(sigma_i_alfa)+"\n")
    #print("epsilon_i_alfa:"+str(epsilon_i_alfa)+"\n")

    for i in range(int(n)):
        for p in range(int(segvector[0,i])):
            d_i_alpha[i,p]=sigma_i_alfa[i,p]*(1-0.12*np.exp(-3*epsilon_i_alfa[i,p]/T))
    #print("d_i_alpha:"+str(d_i_alpha)+"\n")
    return d_i_alpha

def segmentCalcs(x,n,m_i_alfa):
    m_i= np.zeros((n,1),dtype=np.complex_)
    mi_mix= np.zeros((n,1),dtype=np.complex_)
    #m_i= np.zeros((n,1))
    #mi_mix= np.zeros((n,1))
    for i in range(n):
        m_i[i,0]=np.sum(m_i_alfa[i,:])
        mi_mix[i,0] = x[0,i]*m_i[i,0]
    m_i_mix = np.sum(mi_mix)
    #print("\n\nm_i:"+str(m_i))
    #print("\n\nm_i_mix:"+str(m_i_mix))
    return m_i, m_i_mix

def seg_frac(m_i,n,hetsegments,segvector,m_i_alfa):
    z_i_alpha = np.zeros((n,hetsegments),dtype=np.complex_)
    #z_i_alpha = np.zeros((n,hetsegments))
    for i in range(n):
        for p in range(int(segvector[0,i])):
            z_i_alpha[i,p]=m_i_alfa[i,p]/m_i[i,0]
    #print("\n\nz_i_alpha:"+str(z_i_alpha))
    return z_i_alpha

def pack_frac(rho,x,T,n,hetsegments, segvector,m_i,d_i_alfa,z_i_alfa):
    d_i0 = np.zeros((n,hetsegments),dtype=np.complex_)
    d_i1 = np.zeros((n,hetsegments),dtype=np.complex_)
    d_i2 = np.zeros((n,hetsegments),dtype=np.complex_)
    d_i3 = np.zeros((n,hetsegments),dtype=np.complex_)
    #d_i0 = np.zeros((n,hetsegments))
    #d_i1 = np.zeros((n,hetsegments))
    #d_i2 = np.zeros((n,hetsegments))
    #d_i3 = np.zeros((n,hetsegments))

    for i in range(n):
        for p in range(int(segvector[0,i])):
            d_i0[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**0
            d_i1[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**1
            d_i2[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**2
            d_i3[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**3
    d_i_0 = np.zeros((n,1),dtype=np.complex_)
    d_i_1 = np.zeros((n,1),dtype=np.complex_)
    d_i_2 = np.zeros((n,1),dtype=np.complex_)
    d_i_3 = np.zeros((n,1),dtype=np.complex_)
    #d_i_0 = np.zeros((n,1))
    #d_i_1 = np.zeros((n,1))
    #d_i_2 = np.zeros((n,1))
    #d_i_3 = np.zeros((n,1))

    d_i_0= np.sum(d_i0,axis=1,keepdims=True)
    d_i_1= np.sum(d_i1,axis=1,keepdims=True)
    d_i_2= np.sum(d_i2,axis=1,keepdims=True)
    d_i_3= np.sum(d_i3,axis=1,keepdims=True)

    zeta0 = np.zeros((n,1),dtype=np.complex_)
    zeta1 = np.zeros((n,1),dtype=np.complex_)
    zeta2 = np.zeros((n,1),dtype=np.complex_)
    zeta3 = np.zeros((n,1),dtype=np.complex_)
    #zeta0 = np.zeros((n,1))
    #zeta1 = np.zeros((n,1))
    #zeta2 = np.zeros((n,1))
    #zeta3 = np.zeros((n,1))


    for i in range(n):
        zeta0[i,0]=cmath.pi/6*rho*x[0,i]*m_i[i,0]*d_i_0[i,0]
        zeta1[i,0]=cmath.pi/6*rho*x[0,i]*m_i[i,0]*d_i_1[i,0]
        zeta2[i,0]=cmath.pi/6*rho*x[0,i]*m_i[i,0]*d_i_2[i,0]
        zeta3[i,0]=cmath.pi/6*rho*x[0,i]*m_i[i,0]*d_i_3[i,0]
    zeta_0=np.sum(zeta0)
    zeta_1=np.sum(zeta1)
    zeta_2=np.sum(zeta2)
    zeta_3=np.sum(zeta3)

    '''
    print("\n\nd_i0:"+str(d_i0))
    print("\n\nd_i1:"+str(d_i1))
    print("\n\nd_i2:"+str(d_i2))
    print("\n\nd_i3:"+str(d_i3))


    print("\n\nd_i_0:"+str(d_i_0))
    print("\n\nd_i_1:"+str(d_i_1))
    print("\n\nd_i_2:"+str(d_i_2))
    print("\n\nd_i_3:"+str(d_i_3))


    print("\n\nzeta0:"+str(zeta0))
    print("\n\nzeta1:"+str(zeta1))
    print("\n\nzeta2:"+str(zeta2))
    print("\n\nzeta3:"+str(zeta3))

    print("\n\nzeta_0:"+str(zeta_0))
    print("\n\nzeta_1:"+str(zeta_1))
    print("\n\nzeta_2:"+str(zeta_2))
    print("\n\nzeta_3:"+str(zeta_3))
    '''
    return zeta_0,zeta_1,zeta_2,zeta_3

def a_dispersion(rho,x,T,n,segvector,A,m_i,m_i_mix,sigma_ij,epsilon_ij_disp,z_i_alfa,zeta_3):
    #print("\n\nrho:"+str(rho)+"\n\nm_i_mix:"+str(m_i_mix)+"\n\nsigma_ij:"+str(sigma_ij)+"\n\nz_i_alfa:"+str(z_i_alfa))
    #print("\n\nzeta_3"+str(zeta_3)+"\n\nx"+str(x)+"\n\nA"+str(A)+"\n\nepsilon_ij_disp"+str(epsilon_ij_disp))
    a_i=np.zeros((7,1),dtype=np.complex_)
    b_i=np.zeros((7,1),dtype=np.complex_)
    #a_i=np.zeros((7,1))
    #b_i=np.zeros((7,1))

    for v in range(7):
        a_i[v,0]=A[v,0]+(m_i_mix-1)/m_i_mix*A[v,1]+(m_i_mix-1)/m_i_mix*(m_i_mix-2)/m_i_mix*A[v,2]
        b_i[v,0]=A[v,3]+(m_i_mix-1)/m_i_mix*A[v,4]+(m_i_mix-1)/m_i_mix*(m_i_mix-2)/m_i_mix*A[v,5]
    #print("\n\na_i"+str(a_i)+"\n\nb_i"+str(b_i))
    eta = zeta_3
    I1=np.zeros((7,1),dtype=np.complex_)
    I2=np.zeros((7,1),dtype=np.complex_)
    #I1=np.zeros((7,1))
    #I2=np.zeros((7,1))
    for i in range(7):
        I1[i,0]=a_i[i,0]*eta**(i)
        I2[i,0]=b_i[i,0]*eta**(i)
    #print("\n\nI1"+str(I1)+"\n\nI2"+str(I2))
    I_1=np.sum(I1)
    I_2=np.sum(I2)
    #print("\n\nI_1"+str(I_1)+"\n\nI_2"+str(I_2))
    C_1 = (1+m_i_mix*(8*eta-2*eta**2)/(1-eta)**4+(1-m_i_mix)*(20*eta-27*eta**2+12*eta**3-2*eta**4)/((1-eta)*(2-eta))**2)**(-1)
    #print("\n\nC_1"+str(C_1))
    sumlinks=0
    sumrechts=0
    for i in range(n):
        for p in range(int(segvector[0,i])):
            for j in range(n):
                for q in range(int(segvector[0,j])):
                    sumlinks = sumlinks + x[0,i]*x[0,j]*m_i[i,0]*m_i[j,0]*z_i_alfa[i,p]*z_i_alfa[j,q]*epsilon_ij_disp[i,p,j,q]/T[0,0]*sigma_ij[i,p,j,q]**3
                    sumrechts = sumrechts + x[0,i]*x[0,j]*m_i[i,0]*m_i[j,0]*z_i_alfa[i,p]*z_i_alfa[j,q]*(epsilon_ij_disp[i,p,j,q]/T[0,0])**2*sigma_ij[i,p,j,q]**3
    a_disp = -2*math.pi*rho*I_1*sumlinks - math.pi*rho*m_i_mix*C_1*I_2*sumrechts
    #print("\n\na_disp:"+str(a_disp))
    return a_disp

def a_dipole(rho,x,T,n,hetsegments,segvector, m_i, sigma_ij,epsilon_ij,z_i_alfa,miu_i,B,zeta_3):
    eta=zeta_3
    mij = np.zeros((n,hetsegments,n,hetsegments),dtype=np.complex_)
    mijk = np.zeros((n,hetsegments,n,hetsegments,n,hetsegments),dtype=np.complex_)
    #mij = np.zeros((n,hetsegments,n,hetsegments))
    #mijk = np.zeros((n,hetsegments,n,hetsegments,n,hetsegments))

    for i in range(n):
        for p in range(int(segvector[0,i])):
            for j in range(n):
                for q in range(int(segvector[0,j])):
                    mij[i,p,j,q] = (m_i[i,0]*z_i_alfa[i,p]*m_i[j,0]*z_i_alfa[j,q])**(1/2)
                    for s in range(n):
                        for r in range(int(segvector[0,s])):
                            mijk[i,p,j,q,s,r] = ((mij[i,p,j,q]**2)*m_i[s,0]*z_i_alfa[s,r])**(1/3)

    m_ij=np.minimum(mij,2)
    m_ijk =np.minimum(mijk,2)

    a_n_ij = np.zeros((5,n,hetsegments,n,hetsegments),dtype=np.complex_)
    b_n_ij = np.zeros((5,n,hetsegments,n,hetsegments),dtype=np.complex_)
    J_2_ij = np.zeros((5,n,hetsegments,n,hetsegments),dtype=np.complex_)
    c_n_ijk = np.zeros((5,n,hetsegments,n,hetsegments,n,hetsegments),dtype=np.complex_)
    J_3_ijk= np.zeros((5,n,hetsegments,n,hetsegments,n,hetsegments),dtype=np.complex_)

    #a_n_ij = np.zeros((5,n,hetsegments,n,hetsegments))
    #b_n_ij = np.zeros((5,n,hetsegments,n,hetsegments))
    #J_2_ij = np.zeros((5,n,hetsegments,n,hetsegments))
    #c_n_ijk = np.zeros((5,n,hetsegments,n,hetsegments,n,hetsegments))
    #J_3_ijk= np.zeros((5,n,hetsegments,n,hetsegments,n,hetsegments))

    for m in range(5):
        for i in range(n):
            for p in range(int(segvector[0,i])):
                for j in range(n):
                    for q in range(int(segvector[0,j])):
                        a_n_ij[m,i,p,j,q] = B[m,0]+(m_ij[i,p,j,q]-1)/m_ij[i,p,j,q]*B[m,1]+(m_ij[i,p,j,q]-1)/m_ij[i,p,j,q]*(m_ij[i,p,j,q]-2)/m_ij[i,p,j,q]*B[m,2]
                        b_n_ij[m,i,p,j,q] = B[m,3]+(m_ij[i,p,j,q]-1)/m_ij[i,p,j,q]*B[m,4]+(m_ij[i,p,j,q]-1)/m_ij[i,p,j,q]*(m_ij[i,p,j,q]-2)/m_ij[i,p,j,q]*B[m,5]
                        J_2_ij[m,i,p,j,q] = (a_n_ij[m,i,p,j,q]+b_n_ij[m,i,p,j,q]*epsilon_ij[i,p,j,q]/T)*eta**(m)

                        for s in range(n):
                            for r in range(int(segvector[0,s])):
                                c_n_ijk[m,i,p,j,q,s,r]=B[m,6]+(m_ijk[i,p,j,q,s,r]-1)/m_ijk[i,p,j,q,s,r]*B[m,7]+(m_ijk[i,p,j,q,s,r]-1)/m_ijk[i,p,j,q,s,r]*(m_ijk[i,p,j,q,s,r]-2)/m_ijk[i,p,j,q,s,r]*B[m,8]
                                J_3_ijk[m,i,p,j,q,s,r]=c_n_ijk[m,i,p,j,q,s,r]*eta**(m)

    J_2_ij_DD = np.zeros((1,n,hetsegments,n,hetsegments),dtype=np.complex_)
    J_3_ijk_DD = np.zeros((1,n,hetsegments,n,hetsegments,n,hetsegments),dtype=np.complex_)
    #J_2_ij_DD = np.zeros((1,n,hetsegments,n,hetsegments))
    #J_3_ijk_DD = np.zeros((1,n,hetsegments,n,hetsegments,n,hetsegments))


    J_2_ij_DD =np.sum(J_2_ij,axis=0,keepdims=True)
    J_3_ijk_DD =np.sum(J_3_ijk,axis=0,keepdims=True)

    a2_sum = 0
    a3_sum = 0

    for i in range(n):
        for p in range(int(segvector[0,i])):
            for j in range(n):
                for q in range(int(segvector[0,j])):
                    a2_sum= a2_sum+ x[0,i]*x[0,j]*((miu_i[i,p]*miu_i[j,q])**2)/(m_i[i,0]*m_i[j,0])*(J_2_ij_DD[0,i,p,j,q]/((sigma_ij[i,p,j,q]**3)*z_i_alfa[i,p]*z_i_alfa[j,q]))
                    for s in range(n):
                        for r in range(int(segvector[0,s])):
                            a=J_3_ijk_DD[0,i,p,j,q,0,r]
                            a3_sum = a3_sum + x[0,i]*x[0,j]*x[0,s]*(miu_i[i,p]*miu_i[j,q]*miu_i[s,r])**2/(m_i[i,0]*m_i[j,0]*m_i[s,0])*(J_3_ijk_DD[0,i,p,j,q,s,r]/(sigma_ij[i,p,j,q]*sigma_ij[i,p,s,r]*sigma_ij[j,q,s,r])/(z_i_alfa[i,p]*z_i_alfa[j,q]*z_i_alfa[s,r]))
    a_2_dach = -math.pi*rho*(1e4/1.3807/T)**2*a2_sum
    a_3_dach = -4/3*math.pi**2*rho**2*(1e4/1.3807/T)**3*a3_sum
    a_dipol = a_2_dach/(1-a_3_dach/a_2_dach)
    #print("\n\na_dipol:"+str(a_dipol))
    return a_dipol

def complex_partial_derivative(func, var, args,h=1e-60):
    x=args[var]
    if isinstance(x, np.ndarray):
        a=np.zeros(x.shape,dtype=np.complex_)
        for i in range(x.shape[1]):
            #print(str(i)+'\n')
            k=x[0,i]
            #print(args[var][0,i])
            args[var][0,i]+=1j*h
            #print(args[var][0,i])
            a[0,i]=np.real((func(args).imag)/h)
            args[var][0,i]=k
    else:
        args[var]+=1j*h
        #print(args[var])
        #print('Single Iteration')
        a=np.real((func(args).imag)/h)
        args[var]=x
    return a

def partial_derivative(func, var, args,h=0.01):
    x=args[var]
    if isinstance(x, np.ndarray):
        a=np.zeros(x.shape,dtype=np.complex_)
        #a=np.zeros(x.shape)
        for i in range(x.shape[1]):
            k=x[0,i]
            x_minus=k-h
            x_minus_minus=x_minus-h
            x_plus=k+h
            x_plus_plus=x_plus+h
            #print(k)
            args[var][0,i]=x_minus
            #print(args[var][0,i])
            f_minus=func(args)
            args[var][0,i]=x_plus
            #print(args[var][0,i])
            f_plus=func(args)
            args[var][0,i]=x_minus_minus
            f_minus_minus=func(args)
            args[var][0,i]=x_plus_plus
            f_plus_plus=func(args)
            r=-f_plus_plus+8*f_plus-8*f_minus+f_minus_minus
            kl=np.reshape(r/(12*h),(1,1))
            #print(kl.shape)
            a[0,i]= kl
            args[var][0,i]=k
            #print(args[var][0,i])
    else:
        x_minus=x-h
        x_minus_minus=x_minus-h
        x_plus=x+h
        x_plus_plus=x_plus+h
        args[var]=x_minus
        f_minus=func(args)
        args[var]=x_plus
        f_plus=func(args)
        args[var]=x_minus_minus
        f_minus_minus=func(args)
        args[var]=x_plus_plus
        f_plus_plus=func(args)
        a=(-f_plus_plus+8*f_plus-8*f_minus+f_minus_minus)/(12*h)
        args[var]=x
    return a
