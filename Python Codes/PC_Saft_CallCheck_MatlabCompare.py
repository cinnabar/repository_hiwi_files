from PC_Saft_complex_leastSq import PC_Saft
import scipy.io,time
import numpy as np
from pyDOE import *
#mat = scipy.io.loadmat('T_x_Python_run.mat')
#T=mat['T']
#x=mat['x']

t = time.time()
numinputvariables=5
numsamples_initial=10
samplingset_0_inputs=lhs(numinputvariables,numsamples_initial)
T_min=298.15
T_max=343.15

T=(T_max-T_min)*samplingset_0_inputs[:,0]+T_min
T=np.reshape(T,(numsamples_initial,1))
x_cut= np.delete(samplingset_0_inputs, 0, axis=1)
x=x_cut/np.sum(x_cut,axis=1,keepdims=True)
comps = np.array([[9,6,5,0]])
print("\nx:"+str(x))
print("\n\nT:"+str(T))


zrho,f,phi=PC_Saft(comps,T,x)

print("\nzrho:"+str(zrho))
print("\n\nf:"+str(f))
print("\n\nphi:"+str(phi))

print("\n\nThe elapsed Time is "+str(time.time() - t)+" seconds.\n")
