import numpy as np
import math as m

def ackley_call(input):
    out=np.zeros((1,input.shape[1]))
    coeff=np.array([0.1,0.1,0.4])
    a=coeff[0]*40
    b=coeff[1]
    c=coeff[2]*np.pi*5
    d=input.shape[0]
    out=-a*np.exp(-b*np.sqrt(np.sum(input**2,axis=0,keepdims=True)/d)) - np.exp(np.sum(np.cos(c*input),axis=0,keepdims=True)/d) + a + m.exp(1)
    print('The size of Array is:',out.shape)
    print('The array is: ',out)
    return out

o=np.random.randn(2,10)
out=ackley_call(o)
