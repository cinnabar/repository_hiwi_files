from PC_Saft_complex_leastSq import Load_func_necess
import numpy as np
import math, cmath ,time
from calcfiles_for_main import directSubstitution,findDensity,Z_and_rho_OutOfEta
from scipy.optimize import fsolve
from calcfiles_comples_leastSq import fuga_coeff


def main_func(comps,Tin,pc_in):
    m_i_alpha,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij_a,k_ij_b,NC=Load_func_necess(comps)
    for i in range(pc_in.shape[0]):
        zu=pc_in
        #safttimer= time.time()
        T=np.reshape(Tin[i,0],(1,1))
        z=np.reshape(pc_in[i,:],(1,NC)).astype(complex)
        k_ij=k_ij_a + T*k_ij_b

        #Calculations Begin
        guesspol283=np.array([[0.0581111110000000,0.895666667000000 ,0.0462222220000000]])
        guesspol288=np.array([[0.0433750000000000,0.929625000000000, 0.0270000000000000]])
        guesspol298=np.array([[0, 0.0512500000000000,0.923916667000000, 0.0248333330000000]])
        guessorg288=np.array([[0.770750000000000 ,0.152375000000000, 0.0768750000000000]])
        guessorg283=np.array([[0.698333333000000, 0.202333333000000 ,0.0993333330000000]])
        guessorg298=np.array([[0, 0.763666667000000, 0.171916667000000, 0.0644166670000000]])

        x1n=np.reshape(guessorg298[0,1:],(1,3)).astype(complex)
        x2n=np.reshape(guesspol298[0,1:],(1,3)).astype(complex)

        x_org_guess =x1n
        x_pol_guess =x2n
        eta_guess = 0.4

        eta_org=fsolve(findDensity,eta_guess,args=(x_org_guess,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),xtol=1e-6,factor=100)
        Z_rho_org = Z_and_rho_OutOfEta(x_org_guess,m_i_alpha,T,
                        sigma_i_alpha,epsilon_i_alpha,segvector,k,P,eta_org)


        eta_pol = fsolve(findDensity,eta_guess,args=(x_pol_guess,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),xtol=1e-6,factor=100)

        Z_rho_pol  = Z_and_rho_OutOfEta(x_pol_guess,m_i_alpha,T,
                        sigma_i_alpha,epsilon_i_alpha,segvector,k,P,eta_pol)

        x_org,x_pol,Z_rho_org,Z_rho_pol,betas,phi1,phi2 = directSubstitution(T,P,x_org_guess,x_pol_guess,z,np.float64(Z_rho_org),np.float64(Z_rho_pol),
                       k_ij,segvector,NC,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,
                       miu_i,A,B,k)

        phi_org = fuga_coeff(Z_rho_org[1],x_org,m_i_alpha,NC,T,sigma_i_alpha,
                epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z_rho_org[0])
        phi_pol = fuga_coeff(Z_rho_pol[1],x_pol,m_i_alpha,NC,T,sigma_i_alpha,
                epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z_rho_pol[0])

        #Debugging functions
        #a=findDensity([0.002],z,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij)
        #b=Z_and_rho_OutOfEta(x1n,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,k,P,eta)

        #Debugging ends.. remember to delete
        print(x_org,x_pol,phi_org,phi_pol)
        #eta_org=somthing with Find Density and the itehr function which will be written now'''
    return x_org,x_pol

main_func(comps=np.array([[6,5,0]]),Tin=np.array([[320]]),pc_in=np.array([[0.4,0.5,0.1]]))
