import numpy as np
import h5py
import tables
import scipy.io
import math, cmath
from scipy.optimize import least_squares,fsolve
from calcfiles_comples_leastSq import a_residual, a_hardchain, seg_diam, seg_frac, segmentCalcs, pack_frac,a_dispersion, a_dipole
from calcfiles_comples_leastSq import fuga_coeff,Z_rho, complex_partial_derivative

#Comps are named after array positions. In matlab its [7 6 1] so here [6 5 0]
def PC_Saft(comps,Temp,x):

    m_i_alpha,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij_a,k_ij_b,NC=Load_func_necess(comps)
    samplesize=Temp.shape[0]
    phi=np.zeros((samplesize,NC))
    f=np.zeros((samplesize,NC))
    zrho=np.zeros((samplesize,2))
    #k=fuga_coeff(0.0035,x,m_i_alpha,3,Temp,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij_a + Temp*k_ij_b,0.0063)
    #r=a_residual([0.008,x,Temp,A,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,miu_i,B,k_ij_a + Temp*k_ij_b,segvector])
    #print('\n Trollloolll \n')
    #print('a_res:'+ str(r))
    #k= complex_partial_derivative(a_residual,0,[0.008,x,Temp,A,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,miu_i,B,k_ij_a + Temp*k_ij_b,segvector])
    #fuga_coeff(0.003066991426488,x.astype(complex),m_i_alpha,3,Temp,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij_a + Temp*k_ij_b,0.007418891374496)

    for reihe in range(samplesize):
        T=Temp[reihe,0]
        T=np.reshape(T,(1,1))
        xiter1=x[reihe,:]
        xiter1=np.reshape(xiter1,(1,NC)).astype(complex)
        #xiter1=np.reshape(xiter1,(1,NC))

        #print("\nx:"+str(xiter1))
        #print("\n\nT:"+str(T))

        k_ij = k_ij_a + T*k_ij_b

        Z_rho1=fsolve(Z_rho,[0.002,0.008],args=(xiter1,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),xtol=1e-8,factor=100)

        if(Z_rho1[1]<0.001):
            Z_rho1=least_squares(Z_rho,[0.0078,0.003],bounds=([0.001,0.001],[0.01,0.01]),args=([xiter1,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij]))
            Z1=Z_rho1.x[0]
            rho1=Z_rho1.x[1]
            zrho[reihe,:]=np.real(Z_rho1.x)
        else:
            Z1  = Z_rho1[0]
            rho1= Z_rho1[1]
            zrho[reihe,:]=np.real(Z_rho1)


        #Z_rho1=least_squares(Z_rho,x_start,bounds=([0.001,0.003],[0.01,0.01]),
        #args=([xiter1,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij]),
        #max_nfev=1e8,gtol=2.8e-16,ftol=2.8e-16,xtol=2.8e-16,jac='3-point',loss='huber')

        #print(Z_rho(Z_rho1,xiter1,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij))
        phi1=fuga_coeff(rho1,xiter1,m_i_alpha,NC,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z1)
        phi[reihe,:]=np.real(phi1)
        f[reihe,:]=np.real(phi1*xiter1)


    return zrho,f,phi

def Load_func_necess(comps):
    mat = scipy.io.loadmat('./PC_Saft_Files/PC_koeff.mat')
    A=mat['A']
    mat = scipy.io.loadmat('./PC_Saft_Files/PCP_koeff.mat')
    B=mat['B']
    mat = scipy.io.loadmat('./PC_Saft_Files/np_sigma.mat')
    sigma=mat['sigma']
    mat = scipy.io.loadmat('./PC_Saft_Files/np_epsilon.mat')
    epsilon=mat['epsilon']
    mat = scipy.io.loadmat('./PC_Saft_Files/np_segmentZahl.mat')
    m=mat['m']
    mat = scipy.io.loadmat('./PC_Saft_Files/np_miu.mat')
    miu=mat['miu']
    mat = h5py.File('./PC_Saft_Files/segs.mat')
    segs=np.array(mat['segs'])
    mat = scipy.io.loadmat('./PC_Saft_Files/np_kijA.mat')
    kijA=mat['kijA']
    mat = scipy.io.loadmat('./PC_Saft_Files/np_kijB.mat')
    kijB=mat['kijB']

    #np.seterr(all='ignore')

    NC=comps.shape[1]
    sigma_i_alpha=epsilon_i_alpha=m_i_alpha=miu_i=np.zeros((NC,2))
    segvector=np.zeros((NC,1))

    sigma_i_alpha= np.squeeze(sigma[comps][:], axis=0)
    epsilon_i_alpha= np.squeeze(epsilon[comps][:], axis=0)
    m_i_alpha      = np.squeeze(m[comps][:], axis=0)
    miu_i          = np.squeeze(miu[comps][:],axis=0)
    segvector        = segs[0][comps] ;
    #print(segvector.shape)

    maxSegmentNumber = int(np.amax(segvector))
    k_ij_a = np.zeros((NC,maxSegmentNumber,NC,maxSegmentNumber))
    k_ij_b = np.zeros((NC,maxSegmentNumber,NC,maxSegmentNumber))
    for i in range(NC):
        for alpha in range(int(segvector[0,i])):
            for j in range(NC):
                for beta in range(int(segvector[0,j])):
                    k_ij_a[i][alpha][j][beta] =  kijA[2*(comps[0,i])+alpha][2*(comps[0,j])+beta]
                    k_ij_b[i][alpha][j][beta] =  kijB[2*(comps[0,i])+alpha][2*(comps[0,j])+beta]

    k=1.38066e-23
    P = 1e5
    return m_i_alpha,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij_a,k_ij_b,NC
#Trial Call Check
#PC_Saft(comps=np.array([[6,5,0]]),Temp=np.array([[319.56782612]]),x=np.array([[0.5691856 ,0.09331897, 0.33749544]]))
