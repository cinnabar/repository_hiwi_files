import numpy as np
import math,cmath
from scipy.optimize import fsolve
from calcfiles_comples_leastSq import a_residual, a_hardchain, seg_diam, seg_frac, segmentCalcs, pack_frac,a_dispersion, a_dipole
from calcfiles_comples_leastSq import fuga_coeff,Z_rho, complex_partial_derivative

def findDensity(X,x,m_i_alfa,T,sigma_i_alfa,epsilon_i_alfa,segvector,A,k,P,B,miu_i,k_ij):
    eta=X[0]
    n=x.shape[1]
    hetsegments = int(np.amax(segvector))
    d_i_alfa=seg_diam(T,n,hetsegments,segvector,sigma_i_alfa,epsilon_i_alfa);
    m_i, m_i_mix= segmentCalcs(x, n, m_i_alfa)
    z_i_alfa = seg_frac(m_i,n,hetsegments,segvector,m_i_alfa)
    ETA1=np.zeros((n,hetsegments),dtype=np.complex_)

    for i in range(n):
        for p in range(int(segvector[0,i])):
            ETA1[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**3

    eta1=np.sum(ETA1,axis=1,keepdims=True)

    ETA=np.zeros(n,dtype=np.complex_)

    for i in range(n):
        ETA[i]=x[0,i]*m_i[i,0]*eta1[i,0]

    #print("\nETA:"+str(ETA))
    #print("\neta1:"+str(eta1))

    denominator=np.sum(ETA)
    #print("\nd:"+str(denominator))
    rho = eta/(cmath.pi/6 *(denominator))
    d_a_res_rho= complex_partial_derivative(a_residual,0,
    [rho,x,T,A,m_i_alfa,sigma_i_alfa,epsilon_i_alfa,miu_i,B,k_ij,segvector])

    #print("\nd_a_res_:"+str(d_a_res_rho))
    Z=1+rho*d_a_res_rho

    F=np.zeros(1)
    F[0]=np.real(P/1e5-Z*(k*1e30*1e-5)*T*rho)
    return F

def Z_and_rho_OutOfEta(x,m_i_alfa,T,sigma_i_alfa,epsilon_i_alfa,segvector,k,P,eta):
    n=x.shape[1]
    hetsegments = int(np.amax(segvector))
    d_i_alfa=seg_diam(T,n,hetsegments,segvector,sigma_i_alfa,epsilon_i_alfa);
    m_i, m_i_mix= segmentCalcs(x, n, m_i_alfa)
    z_i_alfa = seg_frac(m_i,n,hetsegments,segvector,m_i_alfa)
    ETA1=np.zeros((n,hetsegments),dtype=np.complex_)

    for i in range(n):
        for p in range(int(segvector[0,i])):
            ETA1[i,p]=z_i_alfa[i,p]*d_i_alfa[i,p]**3

    eta1=np.sum(ETA1,axis=1,keepdims=True)

    ETA=np.zeros(n,dtype=np.complex_)

    for i in range(n):
        ETA[i]=x[0,i]*m_i[i,0]*eta1[i,0]

    #print("\nETA:"+str(ETA))
    #print("\neta1:"+str(eta1))

    denominator=np.sum(ETA)
    #print("\nd:"+str(denominator))
    rho = np.real(eta/(cmath.pi/6 *(denominator)))
    Zeta =  np.real(P/1e5/((k*1e25)*T*rho))

    return Zeta,rho

def RachfordRice2Dgen(Phasefracs,z,K):
    nph = Phasefracs.size
    nc=z.size
    z_corr = np.matlib.repmat(z,nph,1)
    numeratorEls = (K-1)*z_corr

    Phasefracs_corr = np.matlib.repmat(Phasefracs,1,nc)
    denominatorSumEls = (K-1)*Phasefracs_corr
    denominatorEls = 1+np.sum(denominatorSumEls,0)
    sumElements = numeratorEls/np.matlib.repmat(denominatorEls,nph,1)
    RHS = np.real(np.sum(sumElements,1,keepdims=True))
    RHS=RHS[0,0]
    return RHS

def directSubstitution(T,P,x1,x2,z,Z_rho1,Z_rho2,k_ij,segvector,NC,m_i_alpha,sigma_i_alpha,epsilon_i_alpha,miu_i,A,B,k):
    xiter1 = x1
    xiter2 = x2
    nit = 0
    resid = 1
    Betas =  np.array([0.44])
    while(nit<1e3 and resid >1e-4):
        nit+=1
        Z_rho1=fsolve(Z_rho,Z_rho1,args=(xiter1,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),xtol=1e-10,factor=100,maxfev=1000)
        Z1  = Z_rho1[0]
        rho1= Z_rho1[1]
        phi1=fuga_coeff(rho1,xiter1,m_i_alpha,NC,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z1)

        Z_rho2=fsolve(Z_rho,Z_rho2,args=(xiter2,m_i_alpha,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,k,P,B,miu_i,k_ij),xtol=1e-10,factor=100,maxfev=1000)
        Z2  = Z_rho2[0]
        rho2= Z_rho2[1]
        phi2=fuga_coeff(rho2,xiter2,m_i_alpha,NC,T,sigma_i_alpha,epsilon_i_alpha,segvector,A,B,miu_i,k_ij,Z2)

        #print(phi1,phi2)

        if (np.real(np.sum(phi2))==float('Inf')):
            phi2=ones(size(phi2))*10000
        if (np.real(np.sum(phi1))==float('Inf')):
            phi1=ones(size(phi1))*10000
        if (np.real(np.sum(phi1))==0):
            phi1=phi1+ones(size(phi1))*1e-20
        if (np.real(np.sum(phi2))==0):
            phi2=phi2+ones(size(phi1))*1e-20

        K= phi2/phi1
        K_ref_org=K
        Phasefracs = Betas
        #RashfordRice Solving
        Betas = fsolve(RachfordRice2Dgen, Phasefracs,args=(z,K),xtol=1e-8,factor=100,maxfev=1000)
        #print(Betas)
        #print(xiter1)
        #RashfordRice Solving
        Betas_corr = np.matlib.repmat(Betas,1,NC) #Convert to PYthon
        denominatorSumEls = (K-1)*Betas_corr
        denominatorEls = 1 + np.sum(denominatorSumEls,0)
        denominatorSum = 1- Betas + Betas*K_ref_org
        x2_new = z/denominatorEls

        x2_new = x2_new/(np.sum(x2_new))
        x1_new = x2_new*K_ref_org
        x1_new = x1_new/(np.sum(x1_new))
        X = np.concatenate((xiter2,xiter1),axis=1)
        X_new = np.concatenate((x2_new,x1_new),axis=1)
        resid = np.linalg.norm((X - X_new),ord=2)
        xiter2 = np.abs(x2_new).astype(complex)
        xiter1 = np.abs(x1_new).astype(complex)


    return xiter1, xiter2,Z_rho1,Z_rho2,Betas,phi1,phi2
