%16.03.2018 Opti Tool Decanter using Surrogate models

% Hard-coded for ternary systems!

% xin must be a vector for ONE input composition xin(1:NC)
% T must be the scalar temperature in K

% IMPORTANT: Order of components
% either decane,dmf,dodecene or decane,dmf,dodecanal

% x0 is the initial value. See in x0-section below for examples

% Variable x:
% x(Phase1)= x(1:NC) 1-3
% x(Phase2)= x(NC+1:NC*2) 4-6
% K=x(NC*2+5)=x(end); 7

% Number of components
NC=length(comps); 3

%% Objective
fun = @(x) 1;

%% Bounds (lb <= x <= ub)
% Non-polar solvent decane (xin(1)) goes to non-polar phase: 
% bounds x(1): 0-xin(1), bounds x(5): xin(1)-1
% Polar solvent dmf (xin(2)) goes to polar phase: 
% bounds x(2): xin(2)-1, bounds x(6): 0-xin(2)
lb=[0;xin(2);zeros(NC-2,1); xin(1);0;zeros(NC-2,1); 0];
ub=[xin(1);1;ones(NC-2,1); 1;xin(2);ones(NC-2,1); 1];

%% Linear Equality Constraints
% Phase compositions must sum up to 1
Aeq = [ones(1,NC), -1*ones(1,NC), 0];
beq = [ 0 ];

%% Linear Inequality Constraints (Ax <= b)
% Minimum distance between x phase 1 and x phase 2
  Aieq = [1,zeros(1,NC-1), -1,zeros(1,NC-1), 0];
  bieq = [ -0.05 ];

%% Nonlinear Equality Constraints
% Isofugacity, distribution coefficient K=x(end)
% load('gprmdl_any.mat','gprMdl_F1')
% load('gprmdl_any.mat','gprMdl_F2')
% load('gprmdl_any.mat','gprMdl_F3')
nlcon = @(x) [  predict(gprMdl_Phi1,[T,x(1:NC)'])*x(1)-predict(gprMdl_Phi1,[T,x(NC+1:NC*2)'])*x(NC+1);...
                predict(gprMdl_Phi2,[T,x(1:NC)'])*x(2)-predict(gprMdl_Phi2,[T,x(NC+1:NC*2)'])*x(NC+2);...
                predict(gprMdl_Phi3,[T,x(1:NC)'])*x(3)-predict(gprMdl_Phi3,[T,x(NC+1:NC*2)'])*x(NC+3);...
                [x(end)*x(1:NC)+(1-x(end))*x(NC+1:NC*2)]];
nlrhs = [   zeros(NC,1);...
            xin'];
nle =   [   zeros(size(nlrhs))];        

%% Options
% No solver options in this example. Maybe it is needed
opts = optiset('solver','ipopt','display','iter');

%% Set up optimization problem
Opt = opti('fun',fun,'bounds',lb,ub,'eq',Aeq,beq,'ineq',Aieq,bieq,'nlmix',nlcon,nlrhs,nle,'options',opts);  

%% Initial point
x0=[0.1 0.89 0.01 0.9 0.05 0.05 0.5]; %Dodecen
%x0=[1 0 0 0 1 0 0.5]; %Dodecanal

%% Solve
[x,fval,exitflag,info] = solve(Opt,x0);