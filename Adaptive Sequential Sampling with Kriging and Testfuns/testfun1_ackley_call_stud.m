function out=testfun1_ackley_call_stud(input)

out=zeros(length(input(:,1)),1);

% Params
coeff=[0.1 0.1 0.4];
a=coeff(1,1)*40;
b=coeff(1,2);
c=coeff(1,3)*pi*5;
d=length(input(1,:));

out=(-a*exp(-b*sqrt(d^-1*sum(input'.^2)))-exp(d^-1*sum(cos(c.*input')))+a+exp(1))';
