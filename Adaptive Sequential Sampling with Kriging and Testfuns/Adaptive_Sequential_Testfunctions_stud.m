addpath('./dace');
%% Observe algorithm:
% Set variable to 1 if you want to plot at every new iteration
%dbclear in Adaptive_Sequential_Testfunctions_stud
observe=0;

%% Choose test function
% 1 Ackley function
% 2 Shekel function
testfun=1;

%% Run
if observe==1
dbstop in Adaptive_Sequential_Testfunctions_stud at 211
end

[Xtest,Ytest,samplingset_input,samplingset_output,sampleset_initial_input,sampleset_initial_output,AlgorithmPerf] =Adaptive_Sequential_Test_stud(observe,testfun);

function [Xtest,Ytest,samplingset_input,samplingset_output,sampleset_initial_input,sampleset_initial_output,AlgorithmPerf] = Adaptive_Sequential_Test_stud(observe,testfun)
%% Start initial step timer
timer_initial=tic;

%% Generating a test set for model validation
no_samples=15000;                           %size of the test set
no_inputVar=2;                              %number of input variables
Xtest=lhsdesign(no_samples,no_inputVar);    %latin hypercube design
Xtest=0+Xtest.*(2-0);                       %input space scaling

% Test functions: Shekel or Ackley
if testfun==1
    Ytest=testfun1_ackley_call_stud(Xtest);
elseif testfun==2
    Ytest=testfun2_shekel_call_stud(Xtest);
end


%% Observe algorithm:
if observe ==1
% Test set plotted as surf plot
tri = delaunay(Xtest(:,1),Xtest(:,2));
figure
h = trisurf(tri, Xtest(:,1), Xtest(:,2), Ytest(:,1),'LineStyle','none');
colormap summer
hold on
end

%% Parameters of the adaptive sequential sampling algorithm
pointsadded=5;              %number of points added per iteration
numcandidates_perrun=100;  %number of candidates per iteration
numsamples_initial=200;     %size of the initial sampling set
numsubsets=5;               %number of subsets
% Caution! numsamples_initial+pointsadded must always be divisible by
% numsubsets in this implementation!

%% Generating the initial sample set - LHS
sampleset_initial_input=lhsdesign(numsamples_initial,no_inputVar);

%input space scaling
sampleset_initial_input=0+sampleset_initial_input.*2;

% Call original model
if testfun==1
    sampleset_initial_output=testfun1_ackley_call_stud(sampleset_initial_input);
elseif testfun==2
    sampleset_initial_output=testfun2_shekel_call_stud(sampleset_initial_input);
end

%% Fit initial overall model and check performance
numsamples=numsamples_initial;
[dmodel01, dmodel01_perf] = dacefit(sampleset_initial_input, sampleset_initial_output, 'regpoly0', 'corrgauss', [10 10], [0.001 0.001],[20 20]);

%Predictions
[y_predicted(:,1), y_predicted_MSE(:,1)] = predictor(Xtest, dmodel01);

% Initial MSE
MSE=mean((y_predicted-Ytest).^2);
pMSE=mean(y_predicted_MSE);

% Initial performance numbers
AlgorithmPerf.MSEactual(1)=MSE;
AlgorithmPerf.MSEpred(1)=pMSE;
     
samplingset_input=sampleset_initial_input;
samplingset_output=sampleset_initial_output;

%Stop initial step timer
AlgorithmPerf.itertime(1)=toc(timer_initial);

% Iteration counter
iter=1;

% Stopping criterion: Stop after 50 iterations
while(iter <50)
% Start iteration timer
timer_iter=tic;

%% Generate subsets
% Randomly pick samples out of sampling set and sort them into subsets
reshapepar=reshape(randperm(numsamples)',[],numsubsets);
for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
sampled_input{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output{laufsubsets}(reihe,:)=samplingset_output(reshapepar(reihe,laufsubsets),:);
end
end

%% Generate subset models and overall model
for ii=1:length(sampled_input)
Xtrain=[];
Ytrain=[];
    for i=1:length(sampled_input)
        if i~=ii
        Xtrain=[Xtrain;sampled_input{i}];
        Ytrain=[Ytrain;sampled_output{i}];
        end
    end
[subsetmodel,~]=dacefit(Xtrain,Ytrain, 'regpoly0', 'corrgauss', [10 10], [0.001 0.001],[20 20]);
[subsetmodels{ii}]=subsetmodel;
end
[overallmodel,~]=dacefit(samplingset_input,samplingset_output, 'regpoly0', 'corrgauss', [10 10], [0.001 0.001],[20 20]);
 
%% Generate inputs for new candidates 
newcandidates_inputs=lhsdesign(numcandidates_perrun,no_inputVar);

%input space scaling
newcandidates_inputs=0+newcandidates_inputs.*2;

%% Sampling criterion 1: Euclidian distance
% for details see Eason/Cremaschi 2014

for i=1:numcandidates_perrun
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    for j=2:numsamples
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2)); %always pick the bigger one
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

%% Sampling criterion 2: Jackknife variance
% for details see Eason/Cremaschi 2014, Kleijnen/van Beers 2004

num_subsets=size(subsetmodels,2); %i=1,...,n_c in Kleijnen et al.
num_candidates=size(newcandidates_inputs,1); %j=1,...,c in Kleijnen et al.

% Calculate overall model predictions
[pred_overall, pred_overall_MSE] = predictor(newcandidates_inputs, overallmodel);

% Calculate subset model predictions
for i=1:num_subsets
    [pred_subsetmodels(:,i), pred_subsetmodels_MSE(:,i)] = predictor(newcandidates_inputs, subsetmodels{i});
end

for j=1:num_candidates
for i=1:num_subsets
pseudovalues(j,i)=num_subsets*pred_overall(j)-(num_subsets-1)*pred_subsetmodels(j,i);
end
end

av_pseudovalue=1/num_subsets*sum(pseudovalues,2);

for i=1:num_subsets
jack_sum_term(:,i)=(pseudovalues(:,i)-av_pseudovalue).^2;
end

var_phi=1/(num_subsets*(num_subsets-1))*sum(jack_sum_term,2); %Jackknife variance values

%% Combined sampling criterion nu
Nu=nnd/max(nnd)+var_phi'/max(var_phi'); 

% Sort nu to get the maximum values
[Nu,I]=sort(Nu,'descend');

% Choose new samples
input_extra=newcandidates_inputs(I(1:pointsadded),:);

%% Add extra points, call original function to calculate actual outputs
samplingset_input=[samplingset_input;input_extra];

if testfun==1
    output_extra=testfun1_ackley_call_stud(input_extra);
elseif testfun==2
    output_extra=testfun2_shekel_call_stud(input_extra);
end

samplingset_output=[samplingset_output;output_extra];

%% Fit overall model and check performance
[dmodel01, dmodel01_perf] = dacefit(samplingset_input, samplingset_output, 'regpoly0', 'corrgauss', [10 10], [0.001 0.001],[20 20]);
[y_predicted(:,1), y_predicted_MSE(:,1)] = predictor(Xtest, dmodel01);

MSE=mean((y_predicted-Ytest).^2);
pMSE=mean(y_predicted_MSE);

% Performance numbers
iter=iter+1; 
AlgorithmPerf.MSEactual(iter)=MSE; % initial time =1, first iteration =2
AlgorithmPerf.MSEpred(iter)=pMSE;

numsamples=numsamples+pointsadded;
AlgorithmPerf.itertime(iter)=toc(timer_iter);

%% Observe algorithm
if observe ==1
% Initial inputs plotted in red
scatter3(samplingset_input(:,1),samplingset_input(:,2),samplingset_output(:,1),'rx')
% New inputs plotted in red
scatter3(sampleset_initial_input(:,1),sampleset_initial_input(:,2),sampleset_initial_output,'bx')
end
end
end

% %% Plotting overall results - copy in new file or run in command line:
% % Test set plotted as surf plot
% tri = delaunay(Xtest(:,1),Xtest(:,2));
% figure
% h = trisurf(tri, Xtest(:,1), Xtest(:,2), Ytest(:,1),'LineStyle','none');
% colormap summer
% 
% hold on
% 
% % New inputs plotted in red
% scatter3(samplingset_input(:,1),samplingset_input(:,2),samplingset_output(:,1),'rx')
% % Initial inputs plotted in blue
% scatter3(sampleset_initial_input(:,1),sampleset_initial_input(:,2),sampleset_initial_output,'bx')
% hold off
% 
% % Performance plot: logarithmic plot of the MSE for each iteration
% figure
% semilogy(AlgorithmPerf.MSEvalue,'x','MarkerSize',8,'LineWidth',1);
% xlabel('Iterations','FontSize',14)
% ylabel('MSE (log scale)','FontSize',14)
