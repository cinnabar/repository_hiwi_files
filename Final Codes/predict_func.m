function out_y=predict_func(form,type,set_no,newcandidates_inputs)
%% predict_func(form,'Kriging'/'GMDH','overall'/'Subsets',1/(1,2,3,4,...)); 
addpath('./GMDH');
addpath('./Kriging');


if(strcmp('Kriging',type))
    addpath('./Kriging');
    gpr_MDL=form{set_no};
    out_y=predict(gpr_MDL,cell2mat(newcandidates_inputs));
    
% elseif(strcmp('Kriging_NoOpti',type))
%     addpath('./Kriging_NoOpti');
%     out_y=predict(form{set_no},cell2mat(newcandidates_inputs));
    
elseif(strcmp('GMDH',type))  
    depth=5;
    out_y=predict_single(form(set_no),newcandidates_inputs,depth);

elseif(strcmp('NN',type))
    nn_network=form{set_no};

    a=cell2mat(newcandidates_inputs);
    
    if(length(a(1,:))~=1)
    a=squeeze(a(:,1:end-1));
    end

   if(iscell(nn_network))
       nn_network=nn_network{1};
   end
    out_y=nn_network(a')';
    
    
end

end