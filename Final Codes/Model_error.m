function [R2,RAAE]=Model_error(type)
tic
display('Model time:');
numsamples_initial=100;
numinputvariables=4;
Finaltrainingset_perPhi=SamplingNew(type,numinputvariables,numsamples_initial);
no_test_set=4*numsamples_initial;


Xtrain=[Finaltrainingset_perPhi{1};Finaltrainingset_perPhi{2}(numsamples_initial+1:end,:);...
    Finaltrainingset_perPhi{3}(numsamples_initial+1:end,:)];


comps = [7,6,1] ;
T=Xtrain(:,1);
x=zeros(size(Xtrain,1),size(Xtrain,2)-1);

for spalte=2:length(comps)+1
x(:,spalte-1)=Xtrain(:,spalte);
end

[~,Ytrain]=SamplingPhiF_PC_SAFT(comps,T,x);


%% Generating New test set
Xtest=lhsdesign(no_test_set,numinputvariables);
% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
T_min=298.15;
T_max=343.15;
            
Xtest(:,1)=(T_max-T_min).*Xtest(:,1)+T_min;
Xtest(:,2:end)=diag(1./sum(Xtest(:,2:end),2))*Xtest(:,2:end);
% Call original model
% Edit
comps = [7,6,1] ;
T=Xtest(:,1);
x=zeros(size(Xtest,1),size(Xtest,2)-1);

for spalte=2:length(comps)+1
x(:,spalte-1)=Xtest(:,spalte);
end

[~,Ytest]=SamplingPhiF_PC_SAFT(comps,T,x);

%% training models on new sample set

addpath('./GMDH');
addpath('./Kriging');

if(strcmp('Kriging',type))
    addpath('./Kriging');
    
      krig_struct=Kriging_optimization_ver2('RAAE',Xtrain,Ytrain,Xtest,Ytest);
     coeff = fitrgp(Xtrain,Ytrain,'BasisFunction',char(krig_struct.XAtMinObjective.krig_type),'KernelFunction',char(krig_struct.XAtMinObjective.kernelfunc_type),...
  'Sigma',krig_struct.XAtMinObjective.sigma1);
    y_predicted=predict(coeff,cell2mat(Xtest));%what to put here?
    
    
elseif(strcmp('GMDH',type))   
    depth=5;
          [R2,~,RAAE,y_p,~]=ChoiceRun(Xtrain,Ytrain,Xtest,Ytest,1,depth);

elseif(strcmp('NN',type))
       [~,RAAE,R2] = NN_training(Xtrain,Ytrain,Xtest,Ytest);
       
end
display('Total time:');

toc

end

