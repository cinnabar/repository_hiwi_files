clear
sam_input=rand(10000,1);
sam_output=4*(sam_input).^3;
test_input=rand(1000,1);
test_output=4*(test_input).^3;
intrain{1}=NN_training(sam_input(1:end/2,:),sam_output(1:end/2,:),sam_input(end/2+1:end,:),sam_output(end/2+1:end,:));
outtrain_res=predict_func(intrain,'NN',1,{test_input});
[~,R2_Ini]= Calc_r2_raae(outtrain_res,test_output,3);
