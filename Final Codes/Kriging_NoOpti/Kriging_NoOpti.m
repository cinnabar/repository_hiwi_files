function [R2,coeff,RAAE,y_predicted]=Kriging_NoOpti(krig_type,kernelfunc_type, Xtrain,Ytrain,Xtest,Ytest)

[y,x]=normali_choice(Xtrain,Ytrain,Xtest,Ytest,'norm');
  x_train=x.train;
  y_train=y.train;
  x_test=x.test;
  y_test=y.test;

coeff = fitrgp(x_train,y_train,'BasisFunction',char(krig_type),'KernelFunction',char(kernelfunc_type));
y_predicted_norm=predict(coeff,x_test);
    y=normali_choice(x_train,y_train,x_test,y_test,'unnorm',y_predicted_norm);
          y_predicted=abs(y.predicted);
          
 sys=size(y_train,2);
     
     for u=1:sys
     RAAE(u)=sum(abs((Ytest(:,u)-y_predicted(:,u))))/(length(Ytest(:,u))*std(Ytest(:,u)));
     R2(u)=1-sum((Ytest(:,u)-y_predicted(:,u)).^2 )/sum((Ytest(:,u)-mean(Ytest(:,u))).^2 );
     end
     

     



