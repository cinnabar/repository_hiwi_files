tic
clear all
no_samples=10000;
no_inputVar=3;
no_outputVar=3;
load('Coeff_Ackley.mat');
Xtest=lhsdesign(no_samples,no_inputVar);
Xtest=diag(1./sum(Xtest,2))*Xtest;
Ytest=ackley_call('single',Xtest,no_outputVar,coeff_Ackley);
save('ackley_samples.mat','Xtest','Ytest');
toc