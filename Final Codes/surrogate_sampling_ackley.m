clear;
tic

tic
addpath('./GMDH');

numoutputvariables=3;
load('Coeff_Ackley.mat');
%%Loading test set
load('ackley_samples.mat');
%%
%%
pointsadded=50;
numcandidates_perrun=1000;
numsamples_initial=250;
numinputvariables=3; %changing for constant Temperature to 3 from 4 for a 3 component system
numsubsets=5;
depth=6;

% Initial sample set - LHS
sampleset_initial_input=lhsdesign(numsamples_initial,numinputvariables);

% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
% T_min=298.15;
% T_max=343.15;
            
% sampleset_initial_input(:,1)=(T_max-T_min).*sampleset_initial_input(:,1)+T_min;
%sampleset_initial_input=[340*ones(numsamples_initial,1) sampleset_initial_input]; %Setting all temperatures to 340 Degs
sampleset_initial_input=diag(1./sum(sampleset_initial_input,2))*sampleset_initial_input;
% Call original model
sampleset_initial_output=ackley_call('single',sampleset_initial_input,numoutputvariables,coeff_Ackley);

%%

numsamples=numsamples_initial;

%%For Constant temp action and matrix inversion problems

[R2,~,~,~,~]=ChoiceRun(sampleset_initial_input,sampleset_initial_output,Xtest,Ytest,1,depth);
to=toc;
for k =1:length(Ytest(1,:))
R2_array{k}.r2value(1)=R2(k);
t{k}.val(1)=toc;
end



for k=1:numoutputvariables

numsamples=numsamples_initial;
samplingset_input=sampleset_initial_input;
samplingset_output=sampleset_initial_output(:,k);

R2=0;
iter=1;
while(R2<0.99 && iter <200)
    tic
reshapepar=reshape(randperm(numsamples)',[],numsubsets);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
sampled_input{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output{laufsubsets}(reihe,:)=samplingset_output(reshapepar(reihe,laufsubsets),:);
end
end

%For matrix inversion problems first column of input is removed
form=trainmodels('GMDH',sampled_input,sampled_output);

newcandidates_inputs=lhsdesign(numcandidates_perrun,numinputvariables);
% newcandidates_inputs(:,1)=(T_max-T_min).*newcandidates_inputs(:,1)+T_min;
newcandidates_inputs=diag(1./sum(newcandidates_inputs,2))*newcandidates_inputs;

nc=numsubsets;

%Eucliedian distance calculation
for i=1:numcandidates_perrun
    
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    
    for j=2:numsamples
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2));
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

%For matrix inversion and GMDH Prediction problems
for i=1:numsubsets
newcandidates_ypseudo1_predsubset{i}=predict_func(form,'GMDH',i,{newcandidates_inputs});
end

phi_prediction=cell2mat(newcandidates_ypseudo1_predsubset);
var_phi=var(phi_prediction');


Nu=nnd/max(nnd)+var_phi/max(var_phi); 

[Nu,I]=sort(Nu,'descend');
input_extra=newcandidates_inputs(I(1:pointsadded),:);

%% Write code to find points and call array of extra points as input_extra(10*4)
samplingset_input=[samplingset_input;input_extra];


output_extra=ackley_call('single',input_extra,numoutputvariables,coeff_Ackley);

samplingset_output=[samplingset_output;output_extra(:,k)];


%% Creating overall model structure by finding R2 value
%editing since temperature is constant. for matrix singularity problems
[R2,~,~,~,~]=ChoiceRun(samplingset_input,samplingset_output,Xtest,Ytest(:,k),1,depth);
iter=iter+1;
R2_array{k}.r2value(iter)=R2;
numsamples=numsamples+pointsadded;
t{k}.val(iter)=toc;

end

sampleset_input_final{k}=samplingset_input;
sampleset_output_final{k}=samplingset_output;


end
toc