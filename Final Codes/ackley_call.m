function out=ackley_call(type,inp,no_outputVar,coeff,no_inputVar,pres)

out=zeros(length(inp(:,1)),no_outputVar);
for i =1:no_outputVar
a=coeff(i,1)*40;
b=coeff(i,2);
c=coeff(i,3)*pi*5; %%Change the call in such a way that the no. of output variables are also included
if(strcmp('single',type))
    d=length(inp(1,:));
out(:,i)=(-a*exp(-b*sqrt(d^-1*sum(inp'.^2)))-exp(d^-1*sum(cos(c.*inp')))+a+exp(1))';
elseif(strcmp('range',type))
    if(no_inputVar==2)
        [X Y]=meshgrid(-inp:pres:inp); 
         sum_1=(X.^2+Y.^2);        
         Cos_sum=(cos(c.*X)+cos(c.*Y));
         d=2;
         out=-a*exp(-b*sqrt(d^-1*sum_1))-exp(d^-1*Cos_sum)+a+exp(1);
         surf(X,Y,out);
    elseif(no_inputVar==3)
        [X Y Z]=meshgrid(-inp:pres:inp);
        sum_1=(X.^2+Y.^2+Z.^2); d=3;
        Cos_sum=(cos(c.*X)+cos(c.*Y)+cos(c.*Z));
         out=-a*exp(-b*sqrt(d^-1*sum_1))-exp(d^-1*Cos_sum)+a+exp(1);
    elseif(no_inputVar==4)
        [X Y Z I]=meshgrid(-inp:pres:inp);
         sum_1=(X.^2+Y.^2+Z.^2+I.^2);
         d=4;
        Cos_sum=(cos(c.*X)+cos(c.*Y)+cos(c.*Z)+cos(c.*I));
        out=-a*exp(-b*sqrt(d^-1*sum_1))-exp(d^-1*Cos_sum)+a+exp(1);
    end
end    
end