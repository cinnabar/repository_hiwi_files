clear;
tic
addpath('./GMDH');

numoutputvariables=3;
load('Coeff_Ackley.mat');
%%Using a new set of Ackley Coefficient
%coeff_Ackley=rand(numoutputvariables,3);

%% Using a different Test set for 15000 points
no_samples=5000;
no_inputVar=3;
Xtest=lhsdesign(no_samples,no_inputVar);
%Xtest=diag(1./sum(Xtest,2))*Xtest;
Ytest=ackley_call('single',Xtest,numoutputvariables,coeff_Ackley);
%%


pointsadded=50;
numcandidates_perrun=250;
numsamples_initial=250;
numinputvariables=no_inputVar; %changing for constant Temperature to 3 from 4 for a 3 component system
numsubsets=5;
depth=6;

% Initial sample set - LHS
sampleset_initial_input=lhsdesign(numsamples_initial,numinputvariables);

% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
% T_min=298.15;
% T_max=343.15;
            
% sampleset_initial_input(:,1)=(T_max-T_min).*sampleset_initial_input(:,1)+T_min;
%sampleset_initial_input=[340*ones(numsamples_initial,1) sampleset_initial_input]; %Setting all temperatures to 340 Degs
%sampleset_initial_input=diag(1./sum(sampleset_initial_input,2))*sampleset_initial_input;
% Call original model
sampleset_initial_output=ackley_call('single',sampleset_initial_input,numoutputvariables,coeff_Ackley);

%%

numsamples=numsamples_initial;

%%For Constant temp action and matrix inversion problems

type={'GMDH','NN'};

z=1;
%for z=2:-1:1
%for z=1:1:2
    
tic    
if(z==1)    
[R2_Ini,~,~,~,~]=ChoiceRun(sampleset_initial_input,sampleset_initial_output,Xtest,Ytest,1,depth);
elseif(z==2)
intrain{1}=NN_training(sampleset_initial_input(1:end/2,:),sampleset_initial_output(1:end/2,:),sampleset_initial_input(end/2+1:end,:),sampleset_initial_output(end/2+1:end,:));
outtrain_res=predict_func(intrain,'NN',1,{Xtest});
[~,R2_Ini]= Calc_r2_raae(outtrain_res,Ytest,3);
end
to1=toc;
    


for k=1:numoutputvariables
   
R2_array{z,k}.r2value(1)=R2_Ini(k);
t{z,k}.val(1)=to1;
     
numsamples=numsamples_initial;
samplingset_input=sampleset_initial_input;
samplingset_output=sampleset_initial_output(:,k);

figure(k);
subplot(2,1,1);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;

subplot(2,1,2);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;

R2=0;
iter=1;
while(R2<0.99 && iter <100)
    tic
reshapepar=reshape(randperm(numsamples)',[],numsubsets);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
sampled_input{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output{laufsubsets}(reihe,:)=samplingset_output(reshapepar(reihe,laufsubsets),:);
end
end

%For matrix inversion problems first column of input is removed
[form]=trainmodels(type(z),sampled_input,sampled_output);

newcandidates_inputs=lhsdesign(numcandidates_perrun,numinputvariables);
% newcandidates_inputs(:,1)=(T_max-T_min).*newcandidates_inputs(:,1)+T_min;
%newcandidates_inputs=diag(1./sum(newcandidates_inputs,2))*newcandidates_inputs;

nc=numsubsets;

%Eucliedian distance calculation
for i=1:numcandidates_perrun
    
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    
    for j=2:numsamples
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2));
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

%For matrix inversion and GMDH Prediction problems
for i=1:numsubsets
newcandidates_ypseudo1_predsubset{i}=predict_func(form,type(z),i,{newcandidates_inputs});
end

[overallmodel]=trainmodels(type(z),{samplingset_input},{samplingset_output});
pred_overall=predict_func(overallmodel,type(z),1,{newcandidates_inputs});



for i=1:numsubsets
pseudovalues(:,i)=numsubsets*pred_overall-(numsubsets-1)*cell2mat(newcandidates_ypseudo1_predsubset(i));
end


av_pseudovalue=1/numsubsets*sum(pseudovalues,2);



for i=1:numsubsets
jack_sum_term(:,i)=(pseudovalues(:,i)-av_pseudovalue).^2;
end

var_phi=1/(numsubsets*(numsubsets-1))*sum(jack_sum_term,2); %Jackknife variance values



% phi_prediction=cell2mat(newcandidates_ypseudo1_predsubset);
% var_phi=var(phi_prediction');


Nu=nnd/max(nnd)+var_phi/max(var_phi); 

[Nu,I]=sort(Nu,'descend');
input_extra=newcandidates_inputs(I(1:pointsadded),:);

%% Write code to find points and call array of extra points as input_extra(10*4)
samplingset_input=[samplingset_input;input_extra];


output_extra=ackley_call('single',input_extra,numoutputvariables,coeff_Ackley);

samplingset_output=[samplingset_output;output_extra(:,k)];


%% Creating overall model structure by finding R2 value
%editing since temperature is constant. for matrix singularity problems
if(z==1)
[R2,~,~,~,~]=ChoiceRun(samplingset_input,samplingset_output,Xtest,Ytest(:,k),1,depth);
elseif(z==2)
intrain{1}=NN_training(samplingset_input(1:end/2,:),samplingset_output(1:end/2,:),samplingset_input(end/2+1:end,:),samplingset_output(end/2+1:end,:));
outtrain_res=predict_func(intrain,'NN',1,{Xtest});
[~,R2]= Calc_r2_raae(outtrain_res,Ytest(:,k),3);
end

iter=iter+1;
R2_array{z,k}.r2value(iter)=R2;
numsamples=numsamples+pointsadded;
t{z,k}.val(iter)=toc;


subplot(2,1,1);
scatter3(samplingset_output(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;


subplot(2,1,2);
if(exist('pl1','var'))
delete(pl1);
end

pl1=scatter3(samplingset_output(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;

end

sampleset_input_final{z,k}=samplingset_input;
sampleset_output_final{z,k}=samplingset_output;


end

toc