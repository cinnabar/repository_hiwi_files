function [ r2,RAAE ] = CalcVal(x_test_unn,y_test_unn,coeff,powerk)

x_test_unn=squeeze(x_test_unn(:,1:end-1));
ind=length(y_test(1,:));


for i=1:length(x_train_unn(1,:))
x_test(:,i)=(x_test_unn(:,i)-min(x_test_unn(:,i)))/(max(x_test_unn(:,i))-min(x_test_unn(:,i)));
end

for i=1:length(y_train_unn(1,:))
y_test(:,i)=(y_test_unn(:,i)-min(y_test_unn(:,i)))/(max(y_test_unn(:,i))-min(y_test_unn(:,i)));
end

    for i =1:powerk
        if(i==1)
            x_test_full=x_test;
        else
            x_test_full=[x_test_full x_test.^i];
        end
    end
    
    for i=2:powerk
        combo=nchoosek(1:ind,i);
    if(length(combo(:,1))~=1)    
        for j=1:length(combo(:,1))
            x_test_full=[x_test_full x_test(:,combo(j,1)).*x_test(:,combo(j,2))];
        end
    end    
    end


for j=1:ind
    Xf_test(j).a(:,1)=ones(length(x_test_full(:,1)),1);    
    l=2;
    for i=1:length(x_test_full(1,:))
        if(coeff(j).p0(i)==1)
            Xf_test(j).a(:,l)=x_test_full(:,i);
            l=l+1;
        end
    end
X_j_test=squeeze(Xf_test(j).a(:,:));
y_predicted(:,j)=X_j_test*coeff(j).Val';
end

%%R2 Calculations
    [r2,RAAE]= Calc_r2_raae(y_predicted,y_test);


for i=1:length(y_predicted(1,:))
y_predicted_unn(:,i)=abs(y_predicted(:,i)*(max(y_test_unn(:,i))-min(y_test_unn(:,i)))+min(y_test_unn(:,i)));
end
    
end

