function out_y = predict_single(coeff,X,depth)


    Val=coeff{1}.Val;
    p0=coeff{1}.p0;
    max_norm_y=coeff{1}.maxnorm_y;
    min_norm_y=coeff{1}.minnorm_y;
    
    
    max_norm_x=coeff{1}.maxnorm_x;
    min_norm_x=coeff{1}.minnorm_x;
    x=X{1};
    x=squeeze(x(:,1:end-1));
    for i=1:length(x(1,:))
    x(:,i)=(x(:,i)-min_norm_x(i))/(max_norm_x(i)-min_norm_x(i));
    end
    
    x_expan = set_expansion(depth,{x});
    x_expan=cell2mat(x_expan(1));
    l=1;
    for i=1:length(p0)
       if(p0(i)==1)
           x_act(:,l)=x_expan(:,i); 
           l=l+1;
       end
    end

x_act=[ones(length(x_act(:,1)),1) x_act];
out_y=x_act*Val';
out_y=out_y*(max_norm_y-min_norm_y)+min_norm_y;


end

