function [r2,coeff,RAAE,y_predicted,RMAE,p0]=regression(p0,x_train_full,x_test_full,y_train,y_test)



k=length(x_train_full(1,:));

ind_y=length(y_train(1,:));

%p0 = vec2mat(p0,k);
if(length(p0(:,1))==1)
p0 = reshape(p0, length(p0)/k, k);
end

for j=1:ind_y
    Xf_train(j).a(:,1)=ones(length(x_train_full(:,1)),1);
    Xf_test(j).a(:,1)=ones(length(x_test_full(:,1)),1);    
    l=2;
    for i=1:k
        if(p0(j,i)==1)
            Xf_train(j).a(:,l)=x_train_full(:,i);
            Xf_test(j).a(:,l)=x_test_full(:,i);
            l=l+1;
        end
    end
X_j_train=squeeze(Xf_train(j).a(:,:));
X_j_test=squeeze(Xf_test(j).a(:,:));
coeff(j).Val= (inv(X_j_train'*X_j_train)*X_j_train'*y_train(:,j))';
y_predicted(:,j)=X_j_test*coeff(j).Val';
coeff(j).p0=p0(j,:);

end


%%R2 Calculations
    [~,r2, RAAE,RMAE]=Calc_r2_raae(y_predicted,y_test,3);
%     r2=-sum(r2); %For minimisation ease
%     RAAE=sum(RAAE);
    
end