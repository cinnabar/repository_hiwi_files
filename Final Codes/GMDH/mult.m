function prod=mult(a)
if(length(a(1,:))==1)
    prod=a(:,1);
else
    prod=a(:,1).*mult(a(:,2:end));
end    
end