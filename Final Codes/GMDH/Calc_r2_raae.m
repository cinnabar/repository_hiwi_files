function [ret,r2,RAAE,RMAE]= Calc_r2_raae(y_predicted,y_test,crit)
    
     for i=1:length(y_test(1,:))
            SSR(i)=sum((y_predicted(:,i)-mean(y_test(:,i))).^2);
     end
            SSE=sum((y_predicted-y_test).^2);  
       
            
    if(crit==1)
        ret=-sum(1-SSE./SSR);
    elseif(crit==2)
        stdev=std(y_test);
        ret=sum((sum(abs(SSE))/length(y_test(:,1)))./(stdev)); 
    elseif(crit==3)%normal
        r2=1-SSE./SSR;
        stdev=std(y_test);
        RAAE=(sum(abs(SSE))/length(y_test(:,1)))./(stdev);
        RMAE=max(abs(y_predicted-y_test))./stdev;
        ret=0;
    end
    
    
    end