function r=multimining(a,lvl)
if(lvl==2)
l=1;
    for i=1:length(a)
        for j=i:length(a)
              r(l,:)=[a(i) a(j)];
              l=l+1;
        end 
    end    
elseif(lvl==3)
l=1;
    for i=1:length(a)
        for j=i:length(a)
            for k=j:length(a)
              r(l,:)=[a(i) a(j) a(k)];
              l=l+1;
              %r=strcat(r,a(i),a(j),a(k),{' '});
            end
        end 
    end
elseif(lvl==4)    
l=1;
    for i=1:length(a)
        for j=i:length(a)
            for k=j:length(a)
              for h=k:length(a)
              r(l,:)=[a(i) a(j) a(k) a(h)];
              l=l+1;
              end
              %r=strcat(r,a(i),a(j),a(k),{' '});
            end
        end   
    end
elseif(lvl==5)
    l=1;
    for i=1:length(a)
        for j=i:length(a)
            for k=j:length(a)
              for h=k:length(a)
                 for m=h:length(a) 
                    r(l,:)=[a(i) a(j) a(k) a(h) a(m)];
                    l=l+1;
                 end
              end
            end
        end   
    end
elseif(lvl==6)
    l=1;
    for i=1:length(a)
        for j=i:length(a)
            for k=j:length(a)
              for h=k:length(a)
                 for m=h:length(a) 
                     for n=m:length(a)
                        r(l,:)=[a(i) a(j) a(k) a(h) a(m) a(n)];
                        l=l+1;
                     end   
                 end
              end
            end
        end   
    end
end

end