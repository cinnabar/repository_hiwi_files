function [r2,coeff,RAAE,y_predicted,RMAE]=ChoiceRun(Xtrain,Ytrain,Xtest,Ytest,Criteria,depth)
%Criteria==1 is to use r2 as the cost function and Criteria==2 to use RAAE as the cost function

if (Criteria ==1 || Criteria==2)
[y,x]=normali_choice(Xtrain,Ytrain,Xtest,Ytest,'norm');
x_train=x.train;
x_test=x.test;
y_train=y.train;
y_test=y.test;

ind_y=length(y_train(1,:));
%ind_x=length(x_train(1,:));

x_expan= set_expansion( depth,{x_train,x_test});
x_train_full=cell2mat(x_expan(1));
x_test_full=cell2mat(x_expan(2));

minf=@(p0)regression_schnell(p0,x_train_full,x_test_full,y_train,y_test,Criteria);    
pos= ga(ind_y*length(x_train_full(1,:)),minf);

[r2,coeff,RAAE,y_predicted,RMAE]=regression(reshape(pos, length(pos)/length(x_train_full(1,:)), length(x_train_full(1,:))),x_train_full,x_test_full,y_train,y_test);
y=normali_choice(Xtrain,Ytrain,Xtest,Ytest,'unnorm',y_predicted);
coeff(1).maxnorm_y=max(Ytrain);
coeff(1).minnorm_y=min(Ytrain);
coeff(1).maxnorm_x=max(Xtrain);
coeff(1).minnorm_x=min(Xtrain);
y_predicted=abs(y.predicted);
 
else
    disp('Wrong Inputs');
end

end
