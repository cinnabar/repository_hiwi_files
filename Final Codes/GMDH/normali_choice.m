function [y,x,mini,maxi]=normali_choice(Xtrain,Ytrain,Xtest,Ytest,what_to_do,y_predicted)

    x_train_unn=Xtrain;
    y_train_unn=Ytrain;
    x_test_unn=Xtest;
    y_test_unn=Ytest;

if(length(x_train_unn(1,:)>2))    
x_train_unn=squeeze(x_train_unn(:,1:end-1));
x_test_unn=squeeze(x_test_unn(:,1:end-1));
end

%%Normalisation
mini.x_train=min(x_train_unn);
mini.x_test=min(x_test_unn);
mini.y_train=min(y_train_unn);
mini.y_test=min(y_test_unn);

maxi.x_train=max(x_train_unn);
maxi.x_test=max(x_test_unn);
maxi.y_train=max(y_train_unn);
maxi.y_test=max(y_test_unn);

if(strcmp(what_to_do,'norm'))
for i=1:length(x_train_unn(1,:))
x.train(:,i)=(x_train_unn(:,i)-mini.x_train(i))/(maxi.x_train(i)-mini.x_train(i));
end

for i=1:length(x_test_unn(1,:))
x.test(:,i)=(x_test_unn(:,i)-mini.x_test(i))/(maxi.x_test(i)-mini.x_test(i));
end

for i=1:length(y_train_unn(1,:))
y.train(:,i)=(y_train_unn(:,i)-mini.y_train(i))/(maxi.y_train(i)-mini.y_train(i));
end

for i=1:length(y_test_unn(1,:))
y.test(:,i)=(y_test_unn(:,i)-mini.y_test(i))/(maxi.y_test(i)-mini.y_test(i));
end



elseif(strcmp(what_to_do,'unnorm'))
   
for i=1:length(y_train_unn(1,:))
y.predicted(:,i)=(y_predicted(:,i))*(maxi.y_train(i)-mini.y_train(i))+mini.y_train(i);
end

end
end
