function x_expan = set_expansion(depth,x)

for x_size=1:length(x)

x_test=x{x_size};
ind_x=length(x_test(1,:));

if(depth>=1)
x_test_full=x_test;
end

if(depth>=2)
combo=multimining(1:ind_x,2);
if(length(combo(:,1))~=1)
    for j=1:length(combo(:,1))
        x_test_full=[x_test_full x_test(:,combo(j,1)).*x_test(:,combo(j,2))];
    end
end 
end

if(depth>=3)
combo=multimining(1:ind_x,3);
if(length(combo(:,1))~=1)
    for j=1:length(combo(:,1))
        x_test_full=[x_test_full x_test(:,combo(j,1)).*x_test(:,combo(j,2)).*x_test(:,combo(j,3))];
    end
end 
end

if(depth>=4)
combo=multimining(1:ind_x,4);
if(length(combo(:,1))~=1)
    for j=1:length(combo(:,1))
        x_test_full=[x_test_full x_test(:,combo(j,1)).*x_test(:,combo(j,2)).*x_test(:,combo(j,3)).*x_test(:,combo(j,4))];
    end
end 
end

if(depth>=5)
combo=multimining(1:ind_x,5);
if(length(combo(:,1))~=1)
    for j=1:length(combo(:,1))
        x_test_full=[x_test_full x_test(:,combo(j,1)).*x_test(:,combo(j,2)).*x_test(:,combo(j,3)).*x_test(:,combo(j,4)).*x_test(:,combo(j,5))];
    end
end 
end

if(x_size==1)
x_expan={x_test_full};
else
x_expan= [x_expan {x_test_full}];   
end

end


end

