figure(2)
% plot(t{1,1}.val)
hold on 
% plot(t{1,2}.val)
% plot(t{1,3}.val)
plot(t{2,1}.val)
plot(t{2,2}.val)
plot(t{2,3}.val)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
legend('t_1 GMDH','t_2 GMDH','t_3 GMDH','t_1 NN','t_2 NN','t_3 NN');

figure(1);
plot(R2_array{1,1}.r2value)
hold on
plot(R2_array{1,2}.r2value)
plot(R2_array{1,3}.r2value)
plot(R2_array{2,1}.r2value)
plot(R2_array{2,2}.r2value)
plot(R2_array{2,3}.r2value)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
legend('R2_1 GMDH','R2_2 GMDH','R2_3 GMDH','R2_1 NN','R2_2 NN','R2_3 NN');
