clear;
tic
addpath('./adimat');
addpath('./pc-saft');
load('5000pcsaft_reuse.mat');

%%PCSAFT
ADiMat_startup;
pointsadded=50;
numcandidates_perrun=1000;
numsamples_initial=250;
numinputvariables=4; 
numoutputvariables=3;
numsubsets=5;
no_test_set=5000;
depth=6;

% Initial sample set - LHS
sampleset_initial_input=lhsdesign(numsamples_initial,numinputvariables);

% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
T_min=298.15;
T_max=343.15;
            
sampleset_initial_input(:,1)=(T_max-T_min).*sampleset_initial_input(:,1)+T_min;
sampleset_initial_input(:,2:end)=diag(1./sum(sampleset_initial_input(:,2:end),2))*sampleset_initial_input(:,2:end);
% Call original model
% Edit
comps = [7,6,1] ;
T=sampleset_initial_input(:,1);
x=zeros(size(sampleset_initial_input,1),size(sampleset_initial_input,2)-1);

for spalte=2:length(comps)+1
x(:,spalte-1)=sampleset_initial_input(:,spalte);
end

[~,sampleset_initial_output]=SamplingPhiF_PC_SAFT(comps,T,x);

% %% Creating a test set for testing overall model
% 
% Xtest=lhsdesign(no_test_set,numinputvariables);
%             
% Xtest(:,1)=(T_max-T_min).*Xtest(:,1)+T_min;
% Xtest(:,2:end)=diag(1./sum(Xtest(:,2:end),2))*Xtest(:,2:end);
% 
% T=Xtest(:,1);
% x=zeros(size(Xtest,1),size(Xtest,2)-1);
% 
% for spalte=2:length(comps)+1
% x(:,spalte-1)=Xtest(:,spalte);
% end
% 
% [~,Ytest]=SamplingPhiF_PC_SAFT(comps,T,x);

%%

numsamples=numsamples_initial;


[R2,~,~,~,~]=ChoiceRun(sampleset_initial_input,sampleset_initial_output,Xtest,Ytest,1,depth);
for k =1:length(Ytest(1,:))
R2_array{k}.r2value(1)=R2(k);
end


for k=1:numoutputvariables

numsamples=numsamples_initial;
samplingset_input=sampleset_initial_input;
samplingset_output=sampleset_initial_output(:,k);

R2=0;
iter=1;
while(R2<0.99 && iter <200)
reshapepar=reshape(randperm(numsamples)',[],numsubsets);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
sampled_input{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output{laufsubsets}(reihe,:)=samplingset_output(reshapepar(reihe,laufsubsets),:);
end
end


form=trainmodels('GMDH',sampled_input,sampled_output);

newcandidates_inputs=lhsdesign(numcandidates_perrun,numinputvariables);
newcandidates_inputs(:,1)=(T_max-T_min).*newcandidates_inputs(:,1)+T_min;
newcandidates_inputs(:,2:end)=diag(1./sum(newcandidates_inputs(:,2:end),2))*newcandidates_inputs(:,2:end);

nc=numsubsets;

%Eucliedian distance calculation
for i=1:numcandidates_perrun
    
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    
    for j=2:numsamples
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2));
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

for i=1:numsubsets
newcandidates_ypseudo1_predsubset{i}=predict_func(form,'GMDH',i,{newcandidates_inputs});
end

phi_prediction=cell2mat(newcandidates_ypseudo1_predsubset);
var_phi=var(phi_prediction');


Nu=nnd/max(nnd)+var_phi/max(var_phi); 

[Nu,I]=sort(Nu,'descend');
input_extra=newcandidates_inputs(I(1:pointsadded),:);

%% Write code to find points and call array of extra points as input_extra(10*4)
samplingset_input=[samplingset_input;input_extra];


T=input_extra(:,1);
x=zeros(size(input_extra,1),size(input_extra,2)-1);

for spalte=2:length(comps)+1
x(:,spalte-1)=input_extra(:,spalte);
end

[~,output_extra]=SamplingPhiF_PC_SAFT(comps,T,x);

samplingset_output=[samplingset_output;output_extra(:,k)];


%% Creating overall model structure by finding R2 value

[R2,~,~,~,~]=ChoiceRun(samplingset_input,samplingset_output,Xtest,Ytest(:,k),1,depth);
iter=iter+1;
R2_array{k}.r2value(iter)=R2;
numsamples=numsamples+pointsadded;
end

sampleset_input_final{k}=samplingset_input;
sampleset_output_final{k}=samplingset_output;


end
toc