clear;
tic
addpath('./adimat');
addpath('./pc-saft');
addpath('./GMDH');




load('5000_constantTemp340K.mat');

%%PCSAFT
ADiMat_startup;

Xtest=Xtest(:,2:end);
numoutputvariables=3;
pointsadded=50;
numcandidates_perrun=1000;
numsamples_initial=250;
numinputvariables=3; %changing for constant Temperature to 3 from 4 for a 3 component system
numsubsets=5;
depth=6;
comps = [7,6,1] ;
% Initial sample set - LHS
sampleset_initial_input=lhsdesign(numsamples_initial,numinputvariables);


            
sampleset_initial_input=diag(1./sum(sampleset_initial_input,2))*sampleset_initial_input;
% Call original model
T=340*ones(length(sampleset_initial_input(:,1)),1);
[~,sampleset_initial_output]=SamplingPhiF_PC_SAFT(comps,T,sampleset_initial_input);

%%

numsamples=numsamples_initial;

%%For Constant temp action and matrix inversion problems

type={'GMDH','NN'};

for z=2:-1:1
%for z=1:1:2
 


tic    
if(z==1)    
[R2_Ini,~,~,~,~]=ChoiceRun(sampleset_initial_input,sampleset_initial_output,Xtest,Ytest,1,depth);
elseif(z==2)
intrain=NN_training(sampleset_initial_input(1:end/2,:),sampleset_initial_output(1:end/2,:),sampleset_initial_input(end/2+1:end,:),sampleset_initial_output(end/2+1:end,:));
outtrain=predict_func(intrain,'NN',1,{Xtest});
[~,R2_Ini]= Calc_r2_raae(outtrain,Ytest,3);
end
to1=toc;
    


for k=1:numoutputvariables
   
R2_array{z,k}.r2value(1)=R2_Ini(k);
t{z,k}.val(1)=to1;
     
numsamples=numsamples_initial;
samplingset_input=sampleset_initial_input;
samplingset_output=sampleset_initial_output(:,k);

figure(k);
subplot(2,2,1);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;

subplot(2,2,2);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;

subplot(2,2,3);
scatter3(Ytest(:,k),Xtest(:,2),Xtest(:,3),'.','y');
hold on;

subplot(2,2,4);
scatter3(Ytest(:,k),Xtest(:,2),Xtest(:,3),'.','y');
hold on;



figure(k*2);
subplot(2,2,1);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;

subplot(2,2,2);
scatter3(Ytest(:,k),Xtest(:,1),Xtest(:,2),'.','y');
hold on;


subplot(2,2,3);
scatter3(Ytest(:,k),Xtest(:,2),Xtest(:,3),'.','y');
hold on;

subplot(2,2,4);
scatter3(Ytest(:,k),Xtest(:,2),Xtest(:,3),'.','y');
hold on;

R2=0;
iter=1;
while(R2<0.99 && iter <50)
    tic
reshapepar=reshape(randperm(numsamples)',[],numsubsets);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
sampled_input{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output{laufsubsets}(reihe,:)=samplingset_output(reshapepar(reihe,laufsubsets),:);
end
end

%For matrix inversion problems first column of input is removed
[form]=trainmodels(type(z),sampled_input,sampled_output);

newcandidates_inputs=lhsdesign(numcandidates_perrun,numinputvariables);
% newcandidates_inputs(:,1)=(T_max-T_min).*newcandidates_inputs(:,1)+T_min;
newcandidates_inputs=diag(1./sum(newcandidates_inputs,2))*newcandidates_inputs;

nc=numsubsets;

%Eucliedian distance calculation
for i=1:numcandidates_perrun
    
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    
    for j=2:numsamples
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2));
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

%For matrix inversion and GMDH Prediction problems
%% calculating overall model predictions
[overallmodel]=trainmodels(type(z),{samplingset_input},{samplingset_output});
pred_overall=predict_func(overallmodel,type(z),1,{newcandidates_inputs});


for i=1:numsubsets
newcandidates_ypseudo1_predsubset(:,i)=predict_func(form,type(z),i,{newcandidates_inputs});
end

for j=1:size(newcandidates_inputs,1)
for i=1:numsubsets
pseudovalues(j,i)=numsubsets*pred_overall(j)-(numsubsets-1)*newcandidates_ypseudo1_predsubset(j,i);
end
end

av_pseudovalue=1/numsubsets*sum(pseudovalues,2);

for i=1:numsubsets
jack_sum_term(:,i)=(pseudovalues(:,i)-av_pseudovalue).^2;
end

var_phi=1/(numsubsets*(numsubsets-1))*sum(jack_sum_term,2); %Jackknife variance values

% phi_prediction=cell2mat(newcandidates_ypseudo1_predsubset);
% var_phi=var(phi_prediction');





Nu=nnd/max(nnd)+var_phi/max(var_phi); 

[Nu,I]=sort(Nu,'descend');
input_extra=newcandidates_inputs(I(1:pointsadded),:);

%% Write code to find points and call array of extra points as input_extra(10*4)
samplingset_input=[samplingset_input;input_extra];

T=340*ones(length(input_extra(:,1)),1);
[~,output_extra]=SamplingPhiF_PC_SAFT(comps,T,input_extra);

samplingset_output=[samplingset_output;output_extra(:,k)];


%% Creating overall model structure by finding R2 value
%editing since temperature is constant. for matrix singularity problems
if(z==1)
[R2,~,~,outtrain,~]=ChoiceRun(samplingset_input,samplingset_output,Xtest,Ytest(:,k),1,depth);
elseif(z==2)
intrain{1}=NN_training(samplingset_input(1:end/2,:),samplingset_output(1:end/2,:),samplingset_input(end/2+1:end,:),samplingset_output(end/2+1:end,:));
outtrain=predict_func(intrain,'NN',1,{Xtest});
[~,R2]= Calc_r2_raae(outtrain,Ytest(:,k),3);
end

iter=iter+1;
R2_array{z,k}.r2value(iter)=R2;
numsamples=numsamples+pointsadded;
t{z,k}.val(iter)=toc;



figure(k);

subplot(2,2,1);
scatter3(samplingset_output(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),'.','k');
hold on;


subplot(2,2,2);
if(exist('pl1','var'))
delete(pl1);
end

pl1=scatter3(samplingset_output(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),'.','k');
hold on;


subplot(2,2,3);
scatter3(samplingset_output(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;


subplot(2,2,4);
if(exist('pl3','var'))
delete(pl3);
end

pl3=scatter3(samplingset_output(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;




figure(k*2);

subplot(2,2,1);
scatter3(outtrain(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),'.','k');
hold on;


subplot(2,2,2);
if(exist('pl2','var'))
delete(pl2);
end

pl2=scatter3(outtrain(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),'.','k');
hold on;


subplot(2,2,3);
scatter3(outtrain(end-pointsadded+1:end,1),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;


subplot(2,2,4);
if(exist('pl4','var'))
delete(pl4);
end

pl4=scatter3(outtrain(end-pointsadded+1:end,k),samplingset_input(end-pointsadded+1:end,2),samplingset_input(end-pointsadded+1:end,3),'.','k');
hold on;


end

sampleset_input_final{z,k}=samplingset_input;
sampleset_output_final{z,k}=samplingset_output;


end

end

toc