function krig_struct=Kriging_optimization_ver2(error_criteria,Xtrain,Ytrain,Xtest,Ytest)
%% Prepare Variables for Bayesian Optimization

%Prepares variable for the initial value of sigma 

sigma1 = optimizableVariable('sigma1',[1e-5,1e5],'Transform','log');
% sigma2 = optimizableVariable('sigma2',[1e-5,1e5],'Transform','log');
% sigma3 = optimizableVariable('sigma3',[1e-5,1e5],'Transform','log');
%Prepares variabe for basis function to decide the type of Kriging, none-Simple, constant-Ordinary,
%linear-Universal
krig_type = optimizableVariable('krig_type',{'none','constant','linear'},'Type','categorical');

%Prepares variabe for kernel function to decide the type of Covariance
%function
kernelfunc_type = optimizableVariable('kernelfunc_type',{'ardexponential','exponential','squaredexponential'},'Type','categorical');



%% Optimize Classifier
if(strcmp(error_criteria,'RAAE')) 
    minfn = @(z)Kriging_opti_ver2(z.sigma1,z.krig_type, z.kernelfunc_type,error_criteria,Xtrain,Ytrain,Xtest,Ytest);
krig_struct = bayesopt(minfn,[sigma1,krig_type,kernelfunc_type],'IsObjectiveDeterministic',true,...
    'AcquisitionFunctionName','expected-improvement-plus');
elseif(strcmp(error_criteria,'R2')) 
    minfn = @(z)((-1)*Kriging_opti_ver2(z.sigma1,z.krig_type,z.kernelfunc_type, error_criteria,Xtrain,Ytrain,Xtest,Ytest));
krig_struct = bayesopt(minfn,[sigma1,krig_type,kernelfunc_type],'IsObjectiveDeterministic',true,...
    'AcquisitionFunctionName','expected-improvement-plus');
end
