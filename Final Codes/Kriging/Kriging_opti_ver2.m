function error=Kriging_opti_ver2(sigma1, krig_type,kernelfunc_type, error_criteria, Xtrain,Ytrain,Xtest,Ytest)

[y,x]=normali_choice(Xtrain,Ytrain,Xtest,Ytest,'norm');
  x_train=x.train(:,:);
  y_train=y.train(:,:);
  x_test=x.test(:,:);
  y_test=y.test(:,:);

  sys=size(x_train,2)-1;
  
%   if sys == 2
% gprMdl_decane = fitrgp(x_train,y_train(:,1),'BasisFunction',char(krig_type),'KernelFunction',char(kernelfunc_type),...
%   'Sigma',sigma1);
% gprMdl_DMF = fitrgp(x_train,y_train(:,2),'BasisFunction',char(krig_type),'KernelFunction',char(kernelfunc_type),...
%   'Sigma',sigma2);
% 
% y_predicted(:,1)=predict(gprMdl_decane, x_test);
% y_predicted(:,2)=predict(gprMdl_DMF, x_test);

%   elseif sys == 3
gprMdl_d = fitrgp(x_train,y_train,'BasisFunction',char(krig_type),'KernelFunction',char(kernelfunc_type),...
  'Sigma',sigma1);

y_predicted(:,1)=predict(gprMdl_d, x_test);
% 
%   end

for u=1:1
 RAAE_error(u)=sum(abs((y_test(:,u)-y_predicted(:,u))))/(length(y_test(:,u))*std(y_test(:,u)));
 R2_error(u)=1-sum((y_test(:,u)-y_predicted(:,u)).^2 )/sum((y_test(:,u)-mean(y_test(:,u))).^2 );  
end

if(strcmp(error_criteria,'RAAE')) 
error=sum(RAAE_error)/sys;
elseif (strcmp(error_criteria,'R2'))
error=sum(R2_error)/sys;
end
  end

% figure();
% plot(x,y,'r.');
% hold on
% plot(x,ypred1,'b');
% plot(x,ypred2,'k','LineWidth',2);
% xlabel('x');
% ylabel('y');
% legend({'data','Initial Fit','Optimized Fit'},'Location','Best');
% title('Impact of Optimization');
% hold off