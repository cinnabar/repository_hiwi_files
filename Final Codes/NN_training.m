function [coeff]=NN_training(Xtrain,Ytrain,Xtest,Ytest)

            inpts=[Xtrain; Xtest];
            targets=[Ytrain; Ytest];
            if(length(inpts(1,:))~=1)
            inpts=squeeze(inpts(:,1:end-1));
            end
            net=feedforwardnet(20);
            net=configure(net,inpts',targets');
            coeff = {train(net,inpts',targets','useParallel','yes','useGPU','yes')};
            
%coeff = {NeuroK(inpts,targets,0.01)};
end     
     