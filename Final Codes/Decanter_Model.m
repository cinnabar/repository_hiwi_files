function [x,fval,exitflag,info]=Decanter_Model(type)

[~,form,~]=Model_error(type);

%31.01.2018 Opti Tool Decanter

% Inputs for the function will be: (x,xin,T,comps)
% Constants: xin,T,comps
comps=[7,6,1];
T=343.15;
xin=[0.3,1-0.3-0.05,0.05];

% Number of components
NC=length(comps);

%% Objective
fun = @(x) 1;

%% Bounds (lb <= x <= ub)
lb=zeros(NC*2+1,1);
ub=ones(NC*2+1,1);

%% Linear Equality Constraints
% Phase compositions must sum up to 1
Aeq = [ 1 1 1 -1 -1 -1 0];
beq = [ 0 ];

%% Nonlinear Equality Constraints
% Isofugacity, distribution coefficient K=x(end)
% load('gprmdl_any.mat','gprMdl_F1')
% load('gprmdl_any.mat','gprMdl_F2')
% load('gprmdl_any.mat','gprMdl_F3')


if(strcmp('GMDH',type))
  
depth=5;

nlcon = @(x) [  predict_single(form(1),[T,x(1:NC)'],depth)*x(1)-predict_single(form(1),[T,x(NC+1:NC*2)'],depth)*x(NC+1);...
                predict_single(form(2),[T,x(1:NC)'],depth)*x(2)-predict_single(form(2),[T,x(NC+1:NC*2)'],depth)*x(NC+2);...
                predict_single(form(3),[T,x(1:NC)'],depth)*x(3)-predict_single(form(3),[T,x(NC+1:NC*2)'],depth)*x(NC+3);...
                [x(end)*x(1:NC)+(1-x(end))*x(NC+1:NC*2)]];
            
elseif(strcmp('Kriging_NoOpti',type))
    
nlcon = @(x) [  predict(form{1},[T,x(1:NC-1)'])*x(1)-predict(form{1},[T,x(NC+1:NC*2-1)'])*x(NC+1);...
                predict(form{2},[T,x(1:NC-1)'])*x(2)-predict(form{2},[T,x(NC+1:NC*2-1)'])*x(NC+2);...
                predict(form{3},[T,x(1:NC-1)'])*x(3)-predict(form{3},[T,x(NC+1:NC*2-1)'])*x(NC+3);...
                [x(end)*x(1:NC)+(1-x(end))*x(NC+1:NC*2)]];

elseif(strcmp('Kriging',type))
    
nlcon = @(x) [  predict(form{1},[T,x(1:NC)'])*x(1)-predict(form{1},[T,x(NC+1:NC*2)'])*x(NC+1);...
                predict(form{2},[T,x(1:NC)'])*x(2)-predict(form{2},[T,x(NC+1:NC*2)'])*x(NC+2);...
                predict(form{3},[T,x(1:NC)'])*x(3)-predict(form{3},[T,x(NC+1:NC*2)'])*x(NC+3);...
                [x(end)*x(1:NC)+(1-x(end))*x(NC+1:NC*2)]];
            
elseif(strcmp('NN',type))

    net_Phi1=form{1};
    net_Phi2=form{2};
    net_Phi3=form{3};

nlcon = @(x) [  net_Phi1([T,x(1:NC)'])*x(1)-net_Phi1([T,x(NC+1:NC*2)'])*x(NC+1);...
                net_Phi2([T,x(1:NC)'])*x(2)-net_Phi2([T,x(NC+1:NC*2)'])*x(NC+2);...
                net_Phi3([T,x(1:NC)'])*x(3)-net_Phi3([T,x(NC+1:NC*2)'])*x(NC+3);...
                [x(end)*x(1:NC)+(1-x(end))*x(NC+1:NC*2)]];
    
end
            
            
nlrhs = [   zeros(NC,1);...
            xin'];
nle =   [  zeros(size(nlrhs))];        

%% Options
opts = optiset('solver','ipopt','display','iter');

%% Set up optimization problem
Opt = opti('fun',fun,'bounds',lb,ub,'eq',Aeq,beq,'nlmix',nlcon,nlrhs,nle,'options',opts);    

%% Initial point
%x0=[0.1 0.89 0.01 0.9 0.05 0.05 0.5]; %Dodecen
% x0=[1 0 0 0 1 0 0.5]; %Tridecanal
x0ph1=[0.01/(0.01+0.99+xin(3)) 0.99/(0.01+0.99+xin(3))  xin(3)/(0.01+0.99+xin(3))];
x0ph2=[0.99/(0.01+0.99+xin(3)) 0.01/(0.01+0.99+xin(3))  xin(3)/(0.01+0.99+xin(3))];
x0K=0.5;
x0=[x0ph1 x0ph2 x0K];

%% Solve
[x,fval,exitflag,info] = solve(Opt,x0);