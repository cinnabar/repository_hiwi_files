function [form]=trainmodels(type,inputset,outputset)
addpath('./GMDH');

 for i=1:length(inputset)
        Xtest=cell2mat(inputset(i));
        Ytest=cell2mat(outputset(i));
        
        
        if(i==length(inputset))
        train_start=1;
        else
        train_start=length(inputset);
        end
       
        Xtrain=cell2mat(inputset(train_start));
        Ytrain=cell2mat(outputset(train_start));
                
        for k=1:length(inputset)
            if(~(i==k || k==train_start))
                Xtrain= [Xtrain; cell2mat(inputset(k))];
                Ytrain=[Ytrain; cell2mat(outputset(k))];
            end
        end
        
        
        
        %%What Form is it???
        
        
 %Kriging
        if(strcmp('Kriging',type))
        addpath('./Kriging');
        krig_struct=Kriging_optimization_ver2('RAAE',Xtrain,Ytrain,Xtest,Ytest);
     coeff = fitrgp(Xtrain,Ytrain,'BasisFunction',char(krig_struct.XAtMinObjective.krig_type),...
         'KernelFunction',char(krig_struct.XAtMinObjective.kernelfunc_type),...
  'Sigma',krig_struct.XAtMinObjective.sigma1);

  %Kriging without Optimization       
%         elseif(strcmp('Kriging_NoOpti',type))
%         addpath('./Kriging_NoOpti');
%      coeff = Kriging_NoOpti('none','ardexponential', Xtrain,Ytrain,Xtest,Ytest);
     
 %GMDH
        elseif(strcmp('GMDH',type))   
            depth=5;
        [~,coeff,~]=ChoiceRun(Xtrain,Ytrain,Xtest,Ytest,1,depth);
 
 %FF Neural Nets
        
        elseif(strcmp('NN',type))
            coeff = NN_training(Xtrain,Ytrain,Xtest,Ytest);
        end
        
        
        form{i}=coeff;
 end
