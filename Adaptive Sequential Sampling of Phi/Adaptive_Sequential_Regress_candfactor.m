%% Start initial step timer
timer_initial=tic;

%% Taking the test set for model validation
no_inputVar=size(set_test_input,2);                           %number of input variables

Xtest_regress=set_test_input;
Ytest_regress=set_test_output_regress;

%% Taking the initial sample set - 100 LHS
sampleset_initial_input=set_initial_train_input;
sampleset_initial_output_regress=set_initial_train_output_regress;

%% Fit initial overall model and check performance
[dmodel01, dmodel01_perf] = dacefit(set_initial_train_input, sampleset_initial_output_regress(:,1), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[dmodel02, dmodel02_perf] = dacefit(set_initial_train_input, sampleset_initial_output_regress(:,2), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[dmodel03, dmodel03_perf] = dacefit(set_initial_train_input, sampleset_initial_output_regress(:,3), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);

%Predictions
[y_predicted(:,1), y_predicted_MSE(:,1)] = predictor(Xtest_regress, dmodel01);
[y_predicted(:,2), y_predicted_MSE(:,2)] = predictor(Xtest_regress, dmodel02);
[y_predicted(:,3), y_predicted_MSE(:,3)] = predictor(Xtest_regress, dmodel03);

% Initial MSE
MSE=mean((y_predicted-Ytest_regress).^2);
pMSE=mean(y_predicted_MSE);
MAE=mean(abs(y_predicted-Ytest_regress));
MRE=mean(abs(y_predicted-Ytest_regress)./Ytest_regress);

% Initial performance numbers
AlgorithmPerf.MSEactual(1,:)=MSE;
AlgorithmPerf.MSEpred(1,:)=pMSE;
AlgorithmPerf.MAE(1,:)=MAE;
AlgorithmPerf.MRE(1,:)=MRE;
AlgorithmPerf.Criteria{1}={};

[AlgorithmPerf.zrho,AlgorithmPerf.f,~]=SamplingPhiF_PC_SAFT([7,6,1],sampleset_initial_input(:,1),sampleset_initial_input(:,2:4));

samplingset_input=sampleset_initial_input;
samplingset_output_regress=sampleset_initial_output_regress;

AlgorithmPerf.samplesize_regress(1)=size(samplingset_input,1);

% Iteration counter
iter=1;

%Stop initial step timer
AlgorithmPerf.itertime(1)=toc(timer_initial);

while size(samplingset_input,1)<stop_size
% Start iteration timer
timer_iter=tic;

%% Generate subsets
disp(['Iteration ',num2str(iter)])

pointsadded=round(pointsadded_factor*size(samplingset_input,1));
numcandidates_perrun=round(pointsadded/numcandidates_factor);

reshapepar_samplesnotfit=mod(size(samplingset_input,1),numsubsets);
reshapepar_randsamplesall=randperm(size(samplingset_input,1))';
reshapepar2=[];
sampled_input_regress=[];
sampled_output_regress=[];
reshapepar=reshape(reshapepar_randsamplesall(1:length(reshapepar_randsamplesall)-reshapepar_samplesnotfit),[],numsubsets);
for laufsubsets=1:numsubsets
    reshapepar2{laufsubsets}=reshapepar(:,laufsubsets);
for reihe=1:size(reshapepar,1)
sampled_input_regress{laufsubsets}(reihe,:)=samplingset_input(reshapepar(reihe,laufsubsets),:);
sampled_output_regress{laufsubsets}(reihe,:)=samplingset_output_regress(reshapepar(reihe,laufsubsets),:);
end
end

for lauf=1:reshapepar_samplesnotfit
    sampled_input_regress{lauf}(size(sampled_input_regress{lauf},1)+1,:)=samplingset_input(reshapepar_randsamplesall(size(reshapepar_randsamplesall,1)-reshapepar_samplesnotfit+lauf),:);
    sampled_output_regress{lauf}(size(sampled_output_regress{lauf},1)+1,:)=samplingset_output_regress(reshapepar_randsamplesall(size(reshapepar_randsamplesall,1)-reshapepar_samplesnotfit+lauf),:);
    reshapepar2{lauf}(size(sampled_output_regress{lauf},1),1)=reshapepar_randsamplesall(size(reshapepar_randsamplesall,1)-reshapepar_samplesnotfit+lauf);
end
    
for laufsubsets=1:numsubsets
    lauf=0;
for reihe=1:size(sampled_input_regress{laufsubsets},1)
    lauf=lauf+1;
    sampled_input_regress{laufsubsets}(lauf,:)=samplingset_input(reshapepar2{laufsubsets}(reihe),:);
    sampled_output_regress{laufsubsets}(lauf,:)=samplingset_output_regress(reshapepar2{laufsubsets}(reihe),:);
end
end



%% Generate subset models and overall model
% Subsetmodels Regress
for ii=1:length(sampled_input_regress)
Xtrain=[];
Ytrain=[];
    for i=1:length(sampled_input_regress)
        if i~=ii
        Xtrain=[Xtrain;sampled_input_regress{i}];
        Ytrain=[Ytrain;sampled_output_regress{i}];
        end
    end
[subsetmodel01,~]=dacefit(Xtrain,Ytrain(:,1), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[subsetmodel02,~]=dacefit(Xtrain,Ytrain(:,2), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[subsetmodel03,~]=dacefit(Xtrain,Ytrain(:,3), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[subsetmodels01{ii}]=subsetmodel01;
[subsetmodels02{ii}]=subsetmodel02;
[subsetmodels03{ii}]=subsetmodel03;
end

samplingset_input_allregress =[];
samplingset_output_allregress =[];
for i=1:length(sampled_input_regress)
    samplingset_input_allregress =[samplingset_input_allregress;sampled_input_regress{i}];
    samplingset_output_allregress =[samplingset_output_allregress;sampled_output_regress{i}];
end

[overallmodel01,~]=dacefit(samplingset_input_allregress,samplingset_output_allregress(:,1), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[overallmodel02,~]=dacefit(samplingset_input_allregress,samplingset_output_allregress(:,2), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[overallmodel03,~]=dacefit(samplingset_input_allregress,samplingset_output_allregress(:,3), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);

%% Generate inputs for new candidates 
newcandidates_inputs=[];

newcandidates_outputs_regress=[];

newcandidates_inputs=rand(numcandidates_perrun,no_inputVar);
        
%input space scaling
newcandidates_inputs(:,2:4)=newcandidates_inputs(:,2:4)./(sum(newcandidates_inputs(:,2:4),2));
newcandidates_inputs(:,1)=298.15+newcandidates_inputs(:,1)*(343.15-298.15);

%% Sampling criterion 1: Euclidian distance
% for details see Eason/Cremaschi 2014
nnd=[];
for i=1:size(newcandidates_inputs,1)%numcandidates_perrun
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(1,:)).^2));
    for j=2:size(samplingset_input,1)
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_input(j,:)).^2)); %always pick the bigger one
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end

%% Sampling criterion 2: Jackknife variance
% for details see Eason/Cremaschi 2014, Kleijnen/van Beers 2004

% Regress:
num_subsets=size(subsetmodels01,2); %i=1,...,n_c in Kleijnen et al.
num_candidates=size(newcandidates_inputs,1); %j=1,...,c in Kleijnen et al.

% Calculate overall model predictions
[pred_overall01, pred_overall01_MSE] = predictor(newcandidates_inputs, overallmodel01);
[pred_overall02, pred_overall02_MSE] = predictor(newcandidates_inputs, overallmodel02);
[pred_overall03, pred_overall03_MSE] = predictor(newcandidates_inputs, overallmodel03);

% Overall Setsize:
num_samples_overall=size(samplingset_output_allregress,1);

% Subsetmodelsize:
num_samples_subsetmodels=[];
for i=1:num_subsets
num_samples_subsetmodels(i)=num_samples_overall-size(sampled_input_regress{i},1);
end

% Calculate subset model predictions
pred_subsetmodels01=[];
pred_subsetmodels01_MSE=[];
pred_subsetmodels02=[];
pred_subsetmodels02_MSE=[];
pred_subsetmodels03=[];
pred_subsetmodels03_MSE=[];
for i=1:num_subsets
    [pred_subsetmodels01(:,i), pred_subsetmodels01_MSE(:,i)] = predictor(newcandidates_inputs, subsetmodels01{i});
    [pred_subsetmodels02(:,i), pred_subsetmodels02_MSE(:,i)] = predictor(newcandidates_inputs, subsetmodels02{i});
    [pred_subsetmodels03(:,i), pred_subsetmodels03_MSE(:,i)] = predictor(newcandidates_inputs, subsetmodels03{i});
end

pseudovalues01=[];
pseudovalues02=[];
pseudovalues03=[];
for j=1:num_candidates
for i=1:num_subsets
pseudovalues01(j,i)=num_samples_overall*pred_overall01(j)-(num_samples_subsetmodels(i))*pred_subsetmodels01(j,i);
pseudovalues02(j,i)=num_samples_overall*pred_overall02(j)-(num_samples_subsetmodels(i))*pred_subsetmodels02(j,i);
pseudovalues03(j,i)=num_samples_overall*pred_overall03(j)-(num_samples_subsetmodels(i))*pred_subsetmodels03(j,i);
end
end

av_pseudovalue01=(num_samples_subsetmodels(1)/sum(num_samples_subsetmodels))*pseudovalues01(:,1);
av_pseudovalue02=(num_samples_subsetmodels(1)/sum(num_samples_subsetmodels))*pseudovalues02(:,1);
av_pseudovalue03=(num_samples_subsetmodels(1)/sum(num_samples_subsetmodels))*pseudovalues03(:,1);
for i=2:num_subsets
av_pseudovalue01=av_pseudovalue01+(num_samples_subsetmodels(i)/sum(num_samples_subsetmodels))*pseudovalues01(:,i);
av_pseudovalue02=av_pseudovalue02+(num_samples_subsetmodels(i)/sum(num_samples_subsetmodels))*pseudovalues02(:,i);
av_pseudovalue03=av_pseudovalue03+(num_samples_subsetmodels(i)/sum(num_samples_subsetmodels))*pseudovalues03(:,i);
end

jack_sum_term01=[];
jack_sum_term02=[];
jack_sum_term03=[];
for i=1:num_subsets
jack_sum_term01(:,i)=1/(num_samples_overall*(num_samples_subsetmodels(i)))*(pseudovalues01(:,i)-av_pseudovalue01).^2;
jack_sum_term02(:,i)=1/(num_samples_overall*(num_samples_subsetmodels(i)))*(pseudovalues02(:,i)-av_pseudovalue02).^2;
jack_sum_term03(:,i)=1/(num_samples_overall*(num_samples_subsetmodels(i)))*(pseudovalues03(:,i)-av_pseudovalue03).^2;
end

var_phi01=[];
var_phi02=[];
var_phi03=[];
var_phi01=sum(jack_sum_term01,2); %Jackknife variance values modified
var_phi02=sum(jack_sum_term02,2); %Jackknife variance values modified
var_phi03=sum(jack_sum_term03,2); %Jackknife variance values modified

%% Combined sampling criterion eta
eta=[];
I=[];
crit_nnd=nnd/max(nnd);
crit_var_K1=var_phi01'/max(var_phi01');
crit_var_K2=var_phi02'/max(var_phi02');
crit_var_K3=var_phi03'/max(var_phi03');


eta=0.5*crit_nnd+0.5*(1/3*crit_var_K1+1/3*crit_var_K2+1/3*crit_var_K3);

% Sort nu to get the maximum values
[eta,I]=sort(eta,'descend');

pointsadded=round(pointsadded_factor*size(samplingset_input,1));

% Choose new samples
input_extra=[];
input_extra=newcandidates_inputs(I(1:pointsadded),:);


%% Add extra points, call original function to calculate actual outputs
%%PC-SAFT Call
comps=[7,6,1];
x=input_extra(:,2:4);
Temp=input_extra(:,1);
[zrho,f,phi]=SamplingPhiF_PC_SAFT(comps,Temp,x);
disp(['Iteration ',num2str(iter)])
output_extra=phi;

extra_in=[];
extra_regress_out=[];
for reihe=1:size(output_extra,1)
        extra_in=[extra_in;input_extra(reihe,:)];
        extra_regress_out=[extra_regress_out;output_extra(reihe,:)];
end

samplingset_input=[samplingset_input;extra_in];
samplingset_output_regress=[samplingset_output_regress;extra_regress_out];

%% Fit overall model and check performance
samplingset_input_allregress =[];
samplingset_output_allregress =[];
for i=1:length(samplingset_input)
    samplingset_input_allregress =[samplingset_input_allregress;samplingset_input(i,:)];
    samplingset_output_allregress =[samplingset_output_allregress;samplingset_output_regress(i,:)];
end
[dmodel01, dmodel01_perf] = dacefit(samplingset_input_allregress, samplingset_output_allregress(:,1), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[dmodel02, dmodel02_perf] = dacefit(samplingset_input_allregress, samplingset_output_allregress(:,2), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);
[dmodel03, dmodel03_perf] = dacefit(samplingset_input_allregress, samplingset_output_allregress(:,3), 'regpoly0', 'corrgauss', [krigpar_start krigpar_start krigpar_start krigpar_start], [krigpar_low krigpar_low krigpar_low krigpar_low],[krigpar_up krigpar_up krigpar_up krigpar_up]);

[y_predicted(:,1), y_predicted_MSE(:,1)] = predictor(Xtest_regress,dmodel01);
[y_predicted(:,2), y_predicted_MSE(:,2)] = predictor(Xtest_regress,dmodel02);
[y_predicted(:,3), y_predicted_MSE(:,3)] = predictor(Xtest_regress,dmodel03);

MSE=mean((y_predicted-Ytest_regress).^2);
pMSE=mean(y_predicted_MSE);
MAE=mean(abs(y_predicted-Ytest_regress));
pMSE=mean(y_predicted_MSE);
MRE=mean(abs(y_predicted-Ytest_regress)./Ytest_regress);

% Performance numbers
iter=iter+1; 

AlgorithmPerf.MSEactual(iter,:)=MSE; % initial time =1, first iteration =2
AlgorithmPerf.MSEpred(iter,:)=pMSE;
AlgorithmPerf.MAE(iter,:)=MAE;
AlgorithmPerf.MRE(iter,:)=MRE;
AlgorithmPerf.Criteria{iter,1}=eta(1:pointsadded);
AlgorithmPerf.samplesize_regress(iter)=size(samplingset_input_allregress,1);
AlgorithmPerf.itertime(iter)=toc(timer_iter);
AlgorithmPerf.zrho=[AlgorithmPerf.zrho;zrho];
AlgorithmPerf.f=[AlgorithmPerf.f;f];

end



