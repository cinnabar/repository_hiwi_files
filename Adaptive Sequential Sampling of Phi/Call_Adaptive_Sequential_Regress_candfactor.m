%% Call Adaptive Sequential method for phi
clear all
clc
krigpar_start=10;
krigpar_low=0.0001;
krigpar_up=20;

load('data.mat','test_lhs_5000_xin_phi')
load('data.mat','test_lhs_5000_xin')

load('data.mat','train_lhs_0050_xin')
load('data.mat','train_lhs_0050_xin_phi')

set_initial_train_input=train_lhs_0050_xin;

set_initial_train_output_regress=train_lhs_0050_xin_phi;

set_test_input=test_lhs_5000_xin;

set_test_output_regress=test_lhs_5000_xin_phi;

stop_size=1000;
pointsadded_factor=0.25;
numcandidates_factor=0.25;
numsubsets=3;               %number of subsets

dbstop if error
Adaptive_Sequential_Regress_candfactor