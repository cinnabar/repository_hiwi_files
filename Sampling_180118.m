%%% Sampling Method as described in Eason, Cremaschi (2014)

%% 1. Initial Sampling Set

% Dimensions
% Edit
numsamples_initial=50;
numinputvariables=4;
numoutputvariables=3;
numsubsets=5;
numcandidates=5;

% Initial set - LHS
samplingset_0_inputs=lhsdesign(numsamples_initial,numinputvariables);

% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
% Edit
minmax_input=[ ...
                298.15, 0, 0, 0;...
                343.15, 1, 1, 1];
            
samplingset_0_inputs=ones(size(samplingset_0_inputs)).*minmax_input(1,:)+(minmax_input(2,:)-minmax_input(1,:)).*samplingset_0_inputs;
samplingset_0_inputs(:,2:end)=samplingset_0_inputs(:,2:end)./(sum(samplingset_0_inputs(:,2:end),2));
% Call original model
% Edit
comps = [7,6,1] ;
T=samplingset_0_inputs(:,1);
x=zeros(size(samplingset_0_inputs,1),size(samplingset_0_inputs,2)-1);
for spalte=2:length(comps)+1
x(:,spalte-1)=[samplingset_0_inputs(:,spalte)];
end

[samplingset_0_output_f,samplingset_0_output_phi]=SamplingPhiF_PC_SAFT(comps,T,x);

%% 2. K Subsets
% Column 1: subset1, ..., entries = selected data (rows of initial data set)
reshapepar=reshape([randperm(numsamples_initial)]',[],numsubsets);

samplingset_0_inputs_subset=cell(numsubsets,1);
samplingset_0_output_f_subset=cell(numsubsets,1);
samplingset_0_output_phi_subset=cell(numsubsets,1);
gprMdl_Phi1_subset=cell(numsubsets,1);
gprMdl_Phi2_subset=cell(numsubsets,1);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
samplingset_0_inputs_subset{laufsubsets}(reihe,:)=samplingset_0_inputs(reshapepar(reihe,laufsubsets),:);
samplingset_0_output_f_subset{laufsubsets}(reihe,:)=samplingset_0_output_f(reshapepar(reihe,laufsubsets),:);
samplingset_0_output_phi_subset{laufsubsets}(reihe,:)=samplingset_0_output_phi(reshapepar(reihe,laufsubsets),:);
end
end

%% 3. Training of K surrogates per output + surrogate using the whole data set
for laufsubsets=1:numsubsets
gprMdl_Phi1_subset{laufsubsets} = fitrgp(samplingset_0_inputs_subset{laufsubsets},samplingset_0_output_phi_subset{laufsubsets}(:,1),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);
gprMdl_Phi2_subset{laufsubsets} = fitrgp(samplingset_0_inputs_subset{laufsubsets},samplingset_0_output_phi_subset{laufsubsets}(:,2),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);
gprMdl_Phi3_subset{laufsubsets} = fitrgp(samplingset_0_inputs_subset{laufsubsets},samplingset_0_output_phi_subset{laufsubsets}(:,3),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);
end

gprMdl_Phi1 = fitrgp(samplingset_0_inputs,samplingset_0_output_phi(:,1),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);
gprMdl_Phi2 = fitrgp(samplingset_0_inputs,samplingset_0_output_phi(:,2),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);
gprMdl_Phi3 = fitrgp(samplingset_0_inputs,samplingset_0_output_phi(:,3),'Basis','constant',...
'PredictMethod','exact','KernelFunction','ardsquaredexponential','Standardize',1);

%% 4. Pseudo values
% New candidates
newcandidates_inputs=lhsdesign(numcandidates,numinputvariables);
newcandidates_inputs=ones(size(newcandidates_inputs)).*minmax_input(1,:)+(minmax_input(2,:)-minmax_input(1,:)).*newcandidates_inputs;
newcandidates_inputs(:,2:end)=newcandidates_inputs(:,2:end)./(sum(newcandidates_inputs(:,2:end),2));
% nc = number of oberservations used in subset-models(?)
% nc=size(samplingset_0_inputs_subset{1},1);
% 08.01.2018: Nope! There are only i=1,....,k subsets, so k
% "subset-surrogates"
nc=numsubsets;
newcandidates_ypseudo1_predoverall=predict(gprMdl_Phi1,newcandidates_inputs);
newcandidates_ypseudo2_predoverall=predict(gprMdl_Phi2,newcandidates_inputs);
newcandidates_ypseudo3_predoverall=predict(gprMdl_Phi3,newcandidates_inputs);

for laufsubsets=1:numsubsets
newcandidates_ypseudo1_predsubset{laufsubsets}=predict(gprMdl_Phi1_subset{laufsubsets},newcandidates_inputs);
newcandidates_ypseudo2_predsubset{laufsubsets}=predict(gprMdl_Phi2_subset{laufsubsets},newcandidates_inputs);
newcandidates_ypseudo3_predsubset{laufsubsets}=predict(gprMdl_Phi3_subset{laufsubsets},newcandidates_inputs);
end

for laufsubsets=1:numsubsets
% Pseudo values
    % Candidates Phi1:
    newcandidates_ypseudo1{laufsubsets}=nc*newcandidates_ypseudo1_predoverall-(nc-1)*newcandidates_ypseudo1_predsubset{laufsubsets};
    % Candidates Phi2:
    newcandidates_ypseudo2{laufsubsets}=nc*newcandidates_ypseudo2_predoverall-(nc-1)*newcandidates_ypseudo2_predsubset{laufsubsets};
    % Candidates Phi3:
    newcandidates_ypseudo3{laufsubsets}=nc*newcandidates_ypseudo3_predoverall-(nc-1)*newcandidates_ypseudo3_predsubset{laufsubsets};
end

newcandidates_ypseudo1_quer=zeros(numcandidates,1);
newcandidates_ypseudo2_quer=zeros(numcandidates,1);
newcandidates_ypseudo3_quer=zeros(numcandidates,1);

for reihe=1:numcandidates
for laufsubsets=1:numsubsets
newcandidates_ypseudo1_quer(reihe)=newcandidates_ypseudo1_quer(reihe)+(newcandidates_ypseudo1{laufsubsets}(reihe));
newcandidates_ypseudo2_quer(reihe)=newcandidates_ypseudo2_quer(reihe)+(newcandidates_ypseudo2{laufsubsets}(reihe));
newcandidates_ypseudo3_quer(reihe)=newcandidates_ypseudo3_quer(reihe)+(newcandidates_ypseudo3{laufsubsets}(reihe));
end
end
newcandidates_ypseudo1_quer=newcandidates_ypseudo1_quer/nc;
newcandidates_ypseudo2_quer=newcandidates_ypseudo2_quer/nc;
newcandidates_ypseudo3_quer=newcandidates_ypseudo3_quer/nc;

% Jackknife variance
jnv_phi1=zeros(numcandidates,1);
jnv_phi2=zeros(numcandidates,1);
jnv_phi3=zeros(numcandidates,1);
for reihe=1:numcandidates
for laufsubsets=1:numsubsets    
jnv_phi1(reihe)=jnv_phi1(reihe)+((newcandidates_ypseudo1{laufsubsets}(reihe,1)-newcandidates_ypseudo1_quer(reihe))^2);
jnv_phi2(reihe)=jnv_phi2(reihe)+((newcandidates_ypseudo2{laufsubsets}(reihe,1)-newcandidates_ypseudo2_quer(reihe))^2);
jnv_phi3(reihe)=jnv_phi3(reihe)+((newcandidates_ypseudo3{laufsubsets}(reihe,1)-newcandidates_ypseudo3_quer(reihe))^2);
end
end


jnv_phi1=jnv_phi1/(nc*(nc-1));
jnv_phi2=jnv_phi2/(nc*(nc-1));
jnv_phi3=jnv_phi3/(nc*(nc-1));
