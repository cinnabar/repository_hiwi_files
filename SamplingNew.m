function Finaltrainingset_perPhi=SamplingNew(type)
tic
%%% Sampling Method as described in Eason, Cremaschi (2014)
addpath('./adimat');
addpath('./pc-saft');

%%PCSAFT
ADiMat_startup;
%%
%% 1. Initial Sampling Set

% Dimensions
% Edit

numsamples_initial=50;
numinputvariables=4; 
numoutputvariables=3;
numsubsets=5;
numcandidates_perrun=100;

% Initial set - LHS
samplingset_0_inputs=lhsdesign(numsamples_initial,numinputvariables);



% Scaling
% Rows 1=min, 2=max; Column 1 =input 1, ...
T_min=298.15;
T_max=343.15;
            
samplingset_0_inputs(:,1)=(T_max-T_min).*samplingset_0_inputs(:,1)+T_min;
samplingset_0_inputs(:,2:end)=diag(1./sum(samplingset_0_inputs(:,2:end),2))*samplingset_0_inputs(:,2:end);
% Call original model
% Edit
comps = [7,6,1] ;
T=samplingset_0_inputs(:,1);
x=zeros(size(samplingset_0_inputs,1),size(samplingset_0_inputs,2)-1);

for spalte=2:length(comps)+1
x(:,spalte-1)=samplingset_0_inputs(:,spalte);
end

[~,samplingset_0_output_phi]=SamplingPhiF_PC_SAFT(comps,T,x);



%% 2. K Subsets
% Column 1: subset1, ..., entries = selected data (rows of initial data set)
reshapepar=reshape([randperm(numsamples_initial)]',[],numsubsets);

samplingset_0_inputs_subset=cell(numsubsets,1);
samplingset_0_output_phi_middle=cell(numsubsets,1);
samplingset_0_output_phi_subset=cell(numoutputvariables,numsubsets);

for laufsubsets=1:numsubsets
for reihe=1:size(reshapepar,1)
samplingset_0_inputs_subset{laufsubsets}(reihe,:)=samplingset_0_inputs(reshapepar(reihe,laufsubsets),:);
samplingset_0_output_phi_middle{laufsubsets}(reihe,:)=samplingset_0_output_phi(reshapepar(reihe,laufsubsets),:);
end
end

for i=1:numsubsets
    for j=1:numoutputvariables
    samplingset_0_output_phi_subset{j,i}(:,1)=samplingset_0_output_phi_middle{i}(:,j);
    end
end




%%

for k=1:numoutputvariables

    
samplingset_perPhi=samplingset_0_inputs;
%% 3. Training of K surrogates per output + surrogate using the whole data set
inputset=samplingset_0_inputs_subset';
outputset=samplingset_0_output_phi_subset(k,:);

form=trainmodels(type,inputset,outputset);


numsample_perPhi=numsamples_initial;
while(numsample_perPhi<100)
%% New candidates
newcandidates_inputs=lhsdesign(numcandidates_perrun,numinputvariables);
newcandidates_inputs(:,1)=(T_max-T_min).*newcandidates_inputs(:,1)+T_min;
newcandidates_inputs(:,2:end)=diag(1./sum(newcandidates_inputs(:,2:end),2))*newcandidates_inputs(:,2:end);

nc=numsubsets;

%Eucliedian distance calculation
for i=1:numcandidates_perrun
    
    nnd(i)=sqrt(sum((newcandidates_inputs(i,:)-samplingset_perPhi(1,:)).^2));
    
    for j=2:numsample_perPhi
    y=sqrt(sum((newcandidates_inputs(i,:)-samplingset_perPhi(j,:)).^2));
    if(y<nnd(i))
        nnd(i)=y;
    end
    end
end


% newcandidates_ypseudo1_predoverall=predict_func(form,'Kriging','overall',1,newcandidates_inputs); %% predict_func(form,'Kriging'/'GMDH','overall'/'Subsets',1/(1,2,3,4,...)); 
%Subsets
for i=1:numsubsets
newcandidates_ypseudo1_predsubset{i}=predict_func(form,type,i,{newcandidates_inputs});
end

phi_prediction=cell2mat(newcandidates_ypseudo1_predsubset);

% %%
% for i=1:numsubsets
% % Pseudo values
%     % Candidates Phi:
%     newcandidates_ypseudo1{i}=nc*newcandidates_ypseudo1_predoverall-(nc-1)*newcandidates_ypseudo1_predsubset{i};
% end
% 
% for i=1:numcandidates
% for j=1:numsubsets
% newcandidates_ypseudo1_quer(i)=newcandidates_ypseudo1_quer(i)+(newcandidates_ypseudo1{j}(i));
% end
% end
% newcandidates_ypseudo1_quer=newcandidates_ypseudo1_quer/nc;

% Jackknife variance
% jnv_phi=zeros(numcandidates,1);
% 
% for reihe=1:numcandidates
% for laufsubsets=1:numsubsets    
% jnv_phi(reihe)=jnv_phi1(reihe)+((newcandidates_ypseudo1{laufsubsets}(reihe,1)-newcandidates_ypseudo1_quer(reihe))^2);
% end
% end
% 
% 
% jnv_phi=jnv_phi1/(nc*(nc-1)); 

%%Change thereafter properly

var_phi=var(phi_prediction');

for i=1:numcandidates_perrun
          Nu(i)=nnd(i)/max(nnd)+var_phi(j)/max(var_phi); 
end

[~,I]=max(Nu);
numsample_perPhi=numsample_perPhi+1;

samplingset_perPhi=[samplingset_perPhi;newcandidates_inputs(I,:)];
end

Finaltrainingset_perPhi{k}=samplingset_perPhi;
end

addpath('./ternplot_folder');
ternplot(samplingset_0_inputs(:,2), samplingset_0_inputs(:,3), samplingset_0_inputs(:,4), 'rx', 'majors', 5);
hold on
%Sampleset for Phi1
Final_sample_points=[Finaltrainingset_perPhi{1};Finaltrainingset_perPhi{2}(numsamples_initial+1:end,:);Finaltrainingset_perPhi{3}(numsamples_initial+1:end,:)];
ternplot(Final_sample_points(:,2), Final_sample_points(:,3), Final_sample_points(:,4), 'b.', 'majors', 5);
%ternplot(A, B)
ternlabel('x1', 'x2', 'x3')
legend('InitialSample', 'NewSample')
title(type)
hold off
toc